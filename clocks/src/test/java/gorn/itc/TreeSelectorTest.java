package gorn.itc;

import org.junit.Test;

import static org.junit.Assert.*;

public class TreeSelectorTest {
    @Test
    public void testCreateRoot() {
        TreeSelector root = TreeSelector.selectAll();
        assertTrue(root.isLeaf());
        assertTrue(root.isEnabled());
    }

    @Test
    public void testCloneRoot() {
        TreeSelector root = TreeSelector.selectAll();
        assertEquals(root, root.clone());
    }

    @Test
    public void testBranch() {
        TreeSelector root = TreeSelector.selectAll();
        root.split();
        assertEquals(root, root.clone());
    }


    @Test
    public void advSplitAdvance() {
        TreeSelector root = TreeSelector.selectAll();
        root.split();
        IntervalTree tree = IntervalTree.createEmpty();
        assertNotNull(root.selectAny());
    }

    @Test
    public void selectLeaf() {
        TreeSelector root = TreeSelector.selectAll();
        IntervalTree tree = IntervalTree.createEmpty();
        assertSame(tree, root.selectAny().allocate(tree));
    }
}
