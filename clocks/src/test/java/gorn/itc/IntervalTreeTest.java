package gorn.itc;

import org.junit.Test;

import static org.junit.Assert.*;

public class IntervalTreeTest {

    @Test
    public void example0() {
        IntervalTree spawn = new IntervalTree(2, IntervalTree.createLeaf(1), IntervalTree.createLeaf(0));
        assertEquals(spawn.toString(), "2 + (1, 0)");
        IntervalTree current = IntervalTree.createLeaf(4);
        assertEquals("4", current.toString());
        assertTrue(spawn.lessThanEquals(current));
    }

    @Test
    public void example1() {
        IntervalTree branch1 = new IntervalTree(1, IntervalTree.createLeaf(1), IntervalTree.createLeaf(0));
        assertEquals(branch1.toString(), "1 + (1, 0)");
        IntervalTree spawn = new IntervalTree(1, IntervalTree.createLeaf(0), branch1);
        assertEquals("1 + (0, 1 + (1, 0))", spawn.toString());
        IntervalTree current = new IntervalTree(4, IntervalTree.createLeaf(0), IntervalTree.createLeaf(0));
        assertEquals("4 + (0, 0)", current.toString());
        assertTrue(spawn.lessThanEquals(current));
    }

}
