package gorn.vc;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

import org.junit.Test;

public class VectorClockTest {

	@Test
	public void example1() {
		VectorClock vc0 = new VectorClock();
		VectorClock vc1 = new VectorClock();
		vc1.advanceTime(1);
		assertFalse(vc0.happensBefore(vc1));
	}

	@Test
	public void example2() {
		VectorClock vc1 = new VectorClock();
		VectorClock vc2 = new VectorClock();
		vc1.setTime(1, 1);
		vc1.setTime(2, 1);
		
		vc2.setTime(1, 2);
		assertFalse(vc1.happensBefore(vc2));
	}

	@Test
	public void example3() {
		VectorClock vc1 = new VectorClock();
		VectorClock vc2 = new VectorClock();
		vc1.setTime(1, 1);
		vc1.setTime(2, 1);
		
		vc2.setTime(1, 2);
		vc2.setTime(2, 1);
		assertTrue(vc1.happensBefore(vc2));
	}

	@Test
	public void example4() {
		// VC{0=>1, 2=>1, 1=>2}, VC{0=>3, 1=>3}
		VectorClock left = new VectorClock();
		left.setTime(0, 1);
		left.setTime(2, 1);
		left.setTime(1, 2);
		VectorClock right = new VectorClock();
		right.setTime(0, 3);
		right.setTime(1, 3);
		assertFalse(left.happensBefore(right));
	}

	@Test
	public void example5() {
		VectorClock v1 = new VectorClock();
		VectorClock v2 = new VectorClock();
		v1.setTime(0, 2);
		v2.setTime(0, 1);
		v2.setTime(1, 1);
		assertFalse(v1.happensBefore(v2));
		assertFalse(v2.happensBefore(v1));
		assertTrue(v1.isConcurrentWith(v2));
	}

	@Test
	public void example6() {
		// <L=VC{2=>2, 6=>2}, G=VC{3=>1, 7=>1}> HB <L=VC{2=>5, 6=>2, 4=>2}, G=VC{3=>1, 7=>1, 5=>1}>
		VectorClock l1 = new VectorClock();
		l1.addTime(2, 2);
		l1.addTime(6, 2);
		VectorClock l2 = new VectorClock();
		l2.addTime(2, 5);
		l2.addTime(6, 2);
		l2.addTime(4, 2);
		assertTrue(l1.happensBefore(l2));
	}
	@Test
	public void example7() {
		// <L=VC{12=>2, 10=>1}
		// PAR
		// <L=VC{14=>2, 10=>4}
		VectorClock l1 = new VectorClock();
		l1.setTime(12, 2);
		l1.setTime(10, 1);

		VectorClock l2 = new VectorClock();
		l2.setTime(14, 2);
		l2.setTime(10, 4);
		assertTrue(l1.isConcurrentWith(l2));
	}

}
