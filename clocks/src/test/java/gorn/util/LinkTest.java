package gorn.util;

import static org.junit.Assert.*;
import gorn.util.Link;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class LinkTest {

	@Test
	public void testLink() {
		Link<Integer> l1 = new Link<Integer>(1);
		Link<Integer> l2 = new Link<>(2);
		l1.next = l2;
		Link<Integer> l3 = new Link<>(3);
		l2.next = l3;
		assertEquals(Arrays.asList(1, 2, 3), Link.asList(l1));
	}

	@Test
	public void constructor() {
	    Assert.assertEquals(Arrays.asList(1,2,3,4), Link.asList(Link.asLink(1,2,3,4)));
    }

    @Test
	public void testSet() {
        Link<Integer> elems = Link.asLink(1,2,3,4,5,6);
        assertEquals(Link.asLink(99,2,3,4,5,6), elems.set(0, 99));
        assertEquals(Link.asLink(1,99,3,4,5,6), elems.set(1, 99));
        assertEquals(Link.asLink(1,2,3,4,5,99), elems.set(5, 99));
        try {
            elems.set(6, 99);
            fail();
        } catch (IndexOutOfBoundsException e ) {
            // OK
        }
        try {
            elems.set(-1, 99);
            fail();
        } catch (IllegalArgumentException e) {
            // OK
        }
    }

    @Test
    public void test1() {
	    int i = 0;
        for (Integer x : Link.iterate(Link.asLink(1,2,3,4,5,6), 3)) {
            i++;
        }
        assertEquals(3, i);
    }
}
