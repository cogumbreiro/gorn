package gorn.vc;

import com.carrotsearch.hppc.IntIntHashMap;
import com.carrotsearch.hppc.IntLongHashMap;
import com.carrotsearch.hppc.cursors.IntIntCursor;

import java.util.TreeMap;

public class MapUtil {
    public static int ZERO = Integer.MIN_VALUE;

    public static int increment(IntIntHashMap vector, int id) {
        return add(vector, id, 1);
    }

    public static int add(IntIntHashMap vector, int id, int amount) {
        return vector.putOrAdd(id, ZERO, amount);
    }

    public static boolean merge(IntIntHashMap left, IntIntHashMap right) {
        if (right.isEmpty()) {
            return false;
        }
        for (IntIntCursor cursor : right) {
            int index = left.indexOf(cursor.key);
            if (index < 0) {
                left.put(cursor.key, cursor.value);
            } else if (cursor.value > left.values[index]) {
                left.values[index] = cursor.value;
            }
        }
        return true;
    }

    public static long asLong(int value) {
        return ((long)value) - ((long)ZERO);
    }

    public static int UInt(int value) {
        if (value < 0) {
            throw new IllegalArgumentException("Expecting a non-negative number");
        }
        return value + ZERO;
    }

    public static String toString(IntIntHashMap vector) {
        // convert from unsigned to signed numbers
        TreeMap<Integer, Long> result = new TreeMap<>();
        for (IntIntCursor cursor : vector) {
            result.put(cursor.key, asLong(cursor.value));
        }
        return result.toString();
    }

}
