package gorn.vc;

import com.carrotsearch.hppc.IntIntHashMap;
import com.carrotsearch.hppc.cursors.IntIntCursor;

/**
 * Straightforward implementation of a Vector Clock.
 * @author Tiago Cogumbreiro
 *
 */
public class VectorClock {
	private final IntIntHashMap vector;

	protected VectorClock(IntIntHashMap view) {
		this.vector = view;
	}
	
	public VectorClock() {
		this(new IntIntHashMap());
	}
	
	public int advanceTime(int id) {
		return MapUtil.increment(vector, id);
	}

	public int addTime(int id, int amount) {
		return MapUtil.add(vector, id, amount);
	}

	public void setTime(int id, int time) {
		vector.put(id, time);
	}
	
	public int getTime(int id) {
		return vector.getOrDefault(id, MapUtil.ZERO);
	}

	public void merge(VectorClock other) {
		MapUtil.merge(this.vector, other.vector);
	}

	public void mergeEntry(int id, int time) {
        int idx = vector.indexOf(id);
        if (idx < 0) {
            vector.put(id, time);
        } else {
            vector.indexReplace(idx, Math.max(vector.indexGet(idx), time));
        }
    }

	/**
	 * This lhs must be a sub-map of the rhs, all elements in the lhs must be <= than all the elements on the rhs,
	 * and there must be an element of the lhs that is strictly greater than an element in the rhs.
	 * @param other
	 * @return
	 */
	public boolean happensBefore(VectorClock other) {
		if (vector.isEmpty()) {
			return false;
		}
		boolean findLt = false;
		if (this.vector.size() > other.vector.size()) {
			return false;
		}
		for (IntIntCursor cursor : this.vector) {
			if (!other.vector.containsKey(cursor.key)) {
				return false;
			}
			int otherValue = other.vector.get(cursor.key);
			if (cursor.value > otherValue) {
				return false;
			}
			findLt = findLt || cursor.value < otherValue;
		}
		return findLt || this.vector.size() < other.vector.size();
	}

	public boolean happensBeforeReflexive(VectorClock other) {
		if (this.vector.size() > other.vector.size()) {
			return false;
		}
		for (IntIntCursor cursor : this.vector) {
			if (!other.vector.containsKey(cursor.key) || cursor.value > other.vector.get(cursor.key)) {
				return false;
			}
		}
		return true;
	}

	public boolean isConcurrentWith(VectorClock other) {
		return ! isReflexiveOrdered(other);
	}

	public boolean isReflexiveOrdered(VectorClock other) {
		return this.happensBeforeReflexive(other) || other.happensBeforeReflexive(this);
	}

	@Override
	public VectorClock clone() {
		return new VectorClock(vector.clone());
	}

	@Override
	public String toString() {
		return MapUtil.toString(vector).replace('=', ':');
	}

	public boolean isEmpty() {
		return vector.isEmpty();
	}

	public void clear() {
		vector.clear();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return false;
		}
		if (!(obj instanceof VectorClock)) {
			return false;
		}
		VectorClock other = (VectorClock) obj;
		return vector.equals(other.vector);
	}

    @Override
    public int hashCode() {
        return vector.hashCode();
    }
}
