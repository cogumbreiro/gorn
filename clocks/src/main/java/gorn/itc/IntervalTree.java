package gorn.itc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Cogumbreiro
 */
class IntervalTree {
    public static final int ZERO = 0;
    private int value;
    private IntervalTree left;
    private IntervalTree right;
    public boolean isLeaf() {
        return left == null && right == null;
    }

    public int getValue() {
        return value;
    }

    public boolean isEmpty() {
        return isLeaf() && value == ZERO;
    }

    protected IntervalTree(int value, IntervalTree left, IntervalTree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public IntervalTree getLeft() {
        return left;
    }

    public IntervalTree getRight() {
        return right;
    }

    public static IntervalTree createEmpty() {
        return createLeaf(ZERO);
    }

    public static IntervalTree createLeaf(int value) {
        return new IntervalTree(value, null, null);
    }

    private static IntervalTree createBranch(IntervalTree left, IntervalTree right) {
        assert left != null;
        assert right != null;
        return new IntervalTree(ZERO, left, right);
    }

    private static IntervalTree createBranch(int value, IntervalTree left, IntervalTree right) {
        assert left != null;
        assert right != null;
        return new IntervalTree(value, left, right);
    }

    public void ensureBranch() {
        if (isLeaf()) {
            left = createEmpty();
            right = createEmpty();
        }
    }

    public boolean simplify() {
        if (isLeaf()) {
            return false;
        }
        if (left.isLeaf() && right.isLeaf()) {
            if (left.value == right.value) {
                value += left.value;
                left = null;
                right = null;
            } else {
                int delta = Math.min(left.value, right.value);
                value += delta;
                left.value -= delta;
                right.value -= delta;
            }
            return true;
        }
        boolean result = false;
        result |= left.simplify();
        result |= right.simplify();
        int delta = Math.min(left.value, right.value);
        value += delta;
        left.value -= delta;
        right.value -= delta;
        return delta > ZERO || result;
    }

    private boolean update() {
        if (isLeaf()) {
            return true;
        }
        if (left.isLeaf() && right.isLeaf()) {
            if (left.value == right.value) {
                value += left.value;
                left = null;
                right = null;
            } else {
                int delta = Math.min(left.value, right.value);
                value += delta;
                left.value -= delta;
                right.value -= delta;
            }
            return true;
        }
        int delta = Math.min(left.value, right.value);
        value += delta;
        left.value -= delta;
        right.value -= delta;
        return delta > ZERO;
    }

    private void selectAll(List<IntervalTree> result, TreeSelector sel) {
        if (sel.isLeaf()) {
            if (sel.isEnabled()) {
                result.add(this);
            }
            return;
        }
        left.selectAll(result, sel.getLeft());
        right.selectAll(result, sel.getRight());
    }

    /**
     * Returns all intervals that match the query.
     * @param query
     * @return
     */
    public List<IntervalTree> selectAll(TreeSelector query) {
        List<IntervalTree> result = new ArrayList<>();
        selectAll(result, query);
        return result;
    }

    private void merge(int accum, int other) {
        if (other == 0) {
            value += accum;
            return;
        }
        if (isLeaf()) {
            value = Math.max(other, value + accum);
            return;
        }
        final int realValue = value + accum;
        final int newOther = other <= realValue ? 0 : other - realValue;
        final int newAccum = realValue <= other ? 0 : realValue - other;
        value = Math.min(other, realValue);
        left.merge(newAccum, newOther);
        right.merge(newAccum, newOther);
        update();
    }

    private void merge(int accum, IntervalTree other, int otherAccum) {
        if (other.isLeaf()) {
            merge(accum, other.value);
            return;
        }
        if (isLeaf()) {
            int otherValue = value + accum;
            other = other.clone();
            left = other.left;
            right = other.right;
            value = other.value;
            merge(otherAccum, otherValue);
            return;
        }
        final int realValue = value + accum;
        final int otherValue = other.value + otherAccum;
        otherAccum = otherValue <= realValue ? 0 : otherValue - realValue;
        accum = realValue <= otherValue ? 0 : realValue - otherValue;
        value = Math.min(otherValue, realValue);
        left.merge(accum, other.left, otherAccum);
        right.merge(accum, other.right, otherAccum);
        update();
    }

    /**
     * Adds all intervals in <code>other</code> to this set of intervals.
     * @param other
     */
    public void merge(IntervalTree other) {
        merge(0, other, 0);
    }

    private int tryLift() {
        return isLeaf() ? ZERO : value;
    }

    private IntervalTree tryGetLeft() {
        return isLeaf() ? this : left;
    }

    private IntervalTree tryGetRight() {
        return isLeaf() ? this : right;
    }

    private boolean lessThanEquals(int accum, IntervalTree other, int otherAccum) {
        if (other == null) {
            return false;
        }
        accum += value; // n_a := value e1 + a
        if (accum <= other.value + otherAccum) { // if (le_dec n_a (value e2 + b))
            if (isLeaf()) {
                return true;
            }
            otherAccum += other.tryLift(); // e_b := tryLift e2 + b
            return left.lessThanEquals(accum, other.tryGetLeft(), otherAccum)
                    && right.lessThanEquals(accum, other.tryGetRight(), otherAccum);
        }
        return false;
    }

    public boolean lessThanEquals(IntervalTree other) {
        return lessThanEquals(0, other, 0);
    }

    public IntervalTree clone() {
        if (isLeaf()) {
            return createLeaf(value);
        }
        return createBranch(value, left.clone(), right.clone());
    }

    public void increment() {
        value++;
    }

    @Override
    public String toString() {
        return isLeaf() ? String.valueOf(value) : (value > ZERO ? (value + " + ") : "") + "("  + left + ", " + right + ")";
    }
}
