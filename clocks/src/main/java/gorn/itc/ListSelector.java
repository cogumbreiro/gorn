package gorn.itc;

import java.util.ArrayList;
import java.util.List;

public class ListSelector implements Selector {
    private ArrayList<TreePath> paths;

    private ListSelector(ArrayList<TreePath> paths) {
        this.paths = paths;
    }

    private ListSelector(TreePath path) {
        this();
        paths.add(path);
    }

    public ListSelector() {
        this.paths = new ArrayList<>();
    }

    @Override
    public boolean merge(Selector obj) {
        ListSelector other = (ListSelector) obj;
        for (TreePath path : other.paths) {
            paths.add(path.clone());
        }
        return other.paths.size() > 0;
    }

    @Override
    public boolean simplify() {
        return false;
    }

    @Override
    public Selector clone() {
        ListSelector copy = new ListSelector();
        copy.merge(this);
        return copy;
    }

    @Override
    public Selector split() {
        // remove the last element until there is only one element left,
        // in which case split this element in half
        return new ListSelector(paths.size() > 1 ? paths.remove(paths.size() - 1) : paths.get(0).split());
    }

    @Override
    public TreePath selectAny() {
        return paths.get(0);
    }

    public static Selector selectAll() {
        return new ListSelector(TreePath.selectAll());
    }
}
