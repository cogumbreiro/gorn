package gorn.itc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Cogumbreiro
 */
class TreeSelector implements Selector {
    private TreeSelector left;
    private TreeSelector right;
    private boolean enabled;

    private TreeSelector(boolean enabled, TreeSelector left, TreeSelector right) {
        this.enabled = enabled;
        this.left = left;
        this.right = right;
    }

    public static TreeSelector selectAll() {
        return createLeaf(true);
    }

    private static TreeSelector createLeaf(boolean isEnabled) {
        return new TreeSelector(isEnabled, null, null);
    }

    private static TreeSelector selectNone() {
        return createLeaf(false);
    }

    private static TreeSelector createBranch(TreeSelector left, TreeSelector right) {
        return new TreeSelector(false, left, right);
    }

    public boolean isLeaf() {
        return left == null && right == null;
    }

    private boolean isLeafEnabled() {
        return isLeaf() && enabled;
    }

    private boolean isLeafDisabled() {
        return isLeaf() && ! enabled;
    }

    protected TreeSelector getLeft() {
        return left;
    }

    public TreeSelector getRight() {
        return right;
    }

    public boolean isEnabled() {
        return enabled;
    }
    /**
     * Simplifies the selector.
     * @return Returns true if the selector has been simplfied.
     */
    @Override
    public boolean simplify() {
        if (isLeaf()) {
            return false;
        }
        boolean result = false;
        result |= left.simplify();
        result |= right.simplify();
        if (left.isLeaf() && right.isLeaf() && left.enabled == right.enabled) {
            enabled = left.enabled;
            right = null;
            left = null;
            return true;
        }
        return result;
    }

    @Override
    public TreeSelector split() {
        if (isLeaf()) {
            if (enabled) {
                left = selectAll();
                right = selectNone();
                return createBranch(selectNone(), selectAll());
            } else {
                return clone();
            }
        }
        if (left.isLeafDisabled()) {
            return createBranch(selectNone(), right.split());
        }
        if (right.isLeafDisabled()) {
            return createBranch(left.split(), selectNone());
        }
        TreeSelector oldRight = right;
        right = selectNone();
        return createBranch(selectNone(), oldRight);
    }

    @Override
    public boolean merge(Selector obj) {
        TreeSelector other = (TreeSelector) obj;
        if (isLeafEnabled() || other.isLeafDisabled()) {
            return false;
        }
        if (isLeafDisabled()) {
            other = other.clone();
            left = other.left;
            right = other.right;
            enabled = other.enabled;
            return true;
        }
        if (other.isLeafEnabled()) {
            left = null;
            right = null;
            enabled = true;
            return true;
        }
        boolean result = false;
        result |= left.merge(other.left);
        result |= right.merge(other.right);
        return result;
    }

    private void selectAll(List<IntervalTree> result, IntervalTree tree) {
        if (tree.isLeaf()) {
            if (isEnabled()) {
                result.add(tree);
            }
            return;
        }
        left.selectAll(result, tree.getLeft());
        right.selectAll(result, tree.getRight());
    }

    /**
     * Returns all intervals in the interval set that this selector.
     * @param tree
     * @return
     */
    public List<IntervalTree> selectAll(IntervalTree tree) {
        List<IntervalTree> result = new ArrayList<>();
        selectAll(result, tree);
        return result;
    }

    private boolean selectAny(TreePath path) {
        if (isLeaf()) {
            if (isEnabled()) {
                return true;
            }
            return false;
        }
        path.navigateLeft();
        if (left.selectAny(path)) {
            return true;
        }
        path.backtrack();
        path.navigateRight();
        return right.selectAny(path);
    }

    @Override
    public TreePath selectAny() {
        TreePath path = new TreePath();
        return selectAny(path) ? path : null;
    }

    @Override
    public TreeSelector clone() {
        return isLeaf() ? createLeaf(enabled) : createBranch(left.clone(), right.clone());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof TreeSelector)) {
            return false;
        }
        TreeSelector other = (TreeSelector) obj;
        if (isLeaf()) {
            return other.isLeaf() && enabled == other.enabled;
        }
        return left.equals(other.left) && right.equals(other.right);
    }

}
