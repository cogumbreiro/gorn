package gorn.itc;

/**
 * @author Tiago Cogumbreiro
 */
public class IntervalClock {
    private Selector selector;
    private IntervalTree tree;
    private IntervalTree selected;
    public IntervalClock() {
        selector = TreeSelector.selectAll();
        tree = IntervalTree.createEmpty();
        updateSelected();
    }

    private IntervalClock(Selector task, IntervalTree intervals) {
        this.selector = task;
        this.tree = intervals;
        updateSelected();
    }

    private void updateSelected() {
        selected = selector.selectAny().allocate(tree);
    }

    public void advanceTime() {
        selected.increment();
    }

    public void merge(IntervalClock other) {
        selector.merge(other.selector);
        tree.merge(other.tree);
        // garbage collect
        selector.simplify();
        tree.simplify();
        // update pointer to selected interval
        updateSelected();
    }

    public IntervalClock split() {
        Selector otherSelector = selector.split();
        IntervalTree otherTree = tree.clone();
        updateSelected();
        return new IntervalClock(otherSelector, otherTree);
    }

    public boolean lessThanEquals(IntervalClock other) {
        return tree.lessThanEquals(other.tree);
    }

    public IntervalClock clone() {
        return new IntervalClock(selector.clone(), tree.clone());
    }

    public boolean isEmpty() {
        return tree.isEmpty();
    }

    @Override
    public String toString() {
        return tree.toString();
    }
}
