package gorn.itc;

public interface Selector {
    Selector split();
    boolean merge(Selector other);
    boolean simplify();
    Selector clone();
    TreePath selectAny();
}
