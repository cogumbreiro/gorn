package gorn.itc;


import com.carrotsearch.hppc.BitSet;

public class TreePath {
    private long size;

    private final BitSet path;

    public TreePath() {
        this(new BitSet(), 0);
    }

    protected TreePath(BitSet path, long size) {
        this.path = path;
        this.size = size;
    }

    public void navigateLeft() {
        path.set(size);
        size++;
    }

    public void navigateRight() {
        size++;
    }

    public void backtrack() {
        path.clear(size - 1); // undo
        size--;
    }

    public IntervalTree find(IntervalTree tree) {
        long nextIndex = path.nextSetBit(0);
        for (long i = 0; i < size && tree != null; i++) {
            if (i == nextIndex) {
                tree = tree.getLeft();
                // get next bit set
                nextIndex = path.nextSetBit(nextIndex + 1);
            } else {
                tree = tree.getRight();
            }
        }
        return tree;
    }

    public IntervalTree allocate(IntervalTree tree) {
        long nextIndex = path.nextSetBit(0);
        for (long i = 0; i < size && tree != null; i++) {
            tree.ensureBranch(); // allocated branches on demand
            if (i == nextIndex) {
                tree = tree.getLeft();
                // get next bit set
                nextIndex = path.nextSetBit(nextIndex + 1);
            } else {
                tree = tree.getRight();
            }
        }
        return tree;
    }

    public static TreePath selectAll() {
        return new TreePath();
    }

    public TreePath clone() {
        return new TreePath((BitSet) path.clone(), size);
    }

    public TreePath split() {
        TreePath copy = clone();
        copy.navigateRight();
        navigateLeft();
        return copy;
    }
}
