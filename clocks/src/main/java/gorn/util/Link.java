package gorn.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A linked list. 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class Link<T> implements Iterable<T> {
	public T element;
	public Link<T> next;

	/**
	 * Construct a list.
	 * @param element
	 * @param next
	 */
	public Link(T element, Link<T> next) {
		this.element = element;
		this.next = next;
	}

	/**
	 * Creates a new link of the list, pointing to a previous list.
	 * @param element
	 */
	public Link(T element) {
		this.element = element;
	}

    /**
     * Sets the n-th position.
     * @param index
     * @param value
     * @return
     */
	public Link<T> set(int index, T value) {
	    if (index < 0) {
	        throw new IllegalArgumentException("Index must be non-negative.");
        }
        if (index == 0) {
	        return new Link<>(value, this.next);
        }
        if (this.next == null) {
	        throw new IndexOutOfBoundsException();
        }
        return new Link<>(this.element, next.set(index - 1, value));
    }

	/**
	 * A simple linked-list iterator.
	 *
	 * @param <T>
	 */
	private static class Iter<T> implements Iterator<T> {
		Link<T> elem;
		
		public Iter(Link<T> elem) {
			this.elem = elem;
		}

		@Override
		public boolean hasNext() {
			return elem != null;
		}
		
		@Override
		public T next() {
			T value = elem.element;
			elem = elem.next;
			return value;
		}

		@Override
		public void remove() {
	        throw new UnsupportedOperationException("remove");
	    }
	}

    /**
     * A simple linked-list iterator.
     *
     * @param <T>
     */
    private static class BoundedIter<T> implements Iterator<T> {
        Link<T> elem;
        private int fuel;

        public BoundedIter(Link<T> elem, int fuel) {
            this.elem = elem;
            this.fuel = fuel;
        }

        @Override
        public boolean hasNext() {
            return fuel > 0 && elem != null;
        }

        @Override
        public T next() {
            T value = elem.element;
            elem = elem.next;
            fuel--;
            return value;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }

	@Override
	public Iterator<T> iterator() {
		return new Iter<>(this);
	}

	public static <T> Iterable<T> iterate(Link<T> link) {
		return link == null ? Collections.EMPTY_LIST : link;
	}

    public static <T> Iterable<T> iterate(Link<T> link, int bound) {
        return link == null ? Collections.EMPTY_LIST : new BoundedLink<>(link, bound);
    }

    public static class BoundedLink<T> implements Iterable<T> {
        private final Link<T> link;
        private final int bound;

        public BoundedLink(Link<T> link, int bound) {
            this.link = link;
            this.bound = bound;
        }

        @Override
        public Iterator<T> iterator() {
            return new BoundedIter<>(link, bound);
        }
    }

    public static <T> List<T> asList(Link<T> link) {
		ArrayList<T> result = new ArrayList<>();
		if (link == null) {
			return result;
		}
		for (T elem : link) {
			result.add(elem);
		}
		return result;
	}

    /**
     * Builds a link.
     * @param elems
     * @return
     */
	public static <T> Link<T> asLink(T... elems) {
        Link<T> elem = null;
        for (int i = elems.length - 1; i >= 0; i--) {
            elem = new Link<>(elems[i], elem);
        }
        return elem;
    }

    @Override
    public boolean equals(Object obj) {
	    if (obj == null) {
	        return false;
        }
        if (!(obj instanceof Link<?>)) {
	        return false;
        }
        Link<?> link1 = this;
	    Link<?> link2 = (Link<?>) obj;
        for (; link1 != null && link2 != null; link1 = link1.next, link2 = link2.next) {
            if (link1.element != null && ! link1.element.equals(link2.element)) {
                return false;
            }
        }
        return link1 == null && link2 == null;
    }

    @Override
	public String toString() {
		ArrayList<T> result = new ArrayList<>();
		for (T elem : this) {
			result.add(elem);
		}
		return Link.asList(this).toString();
	}
}