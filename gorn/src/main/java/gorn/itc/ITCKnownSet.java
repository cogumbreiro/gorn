package gorn.itc;

import gorn.sif.KnownSet;

import java.util.Collection;

public class ITCKnownSet<T> implements KnownSet<T> {

	private final IntervalClock spawnPoint;
	private final IntervalClock current;
    private boolean doInit = true;

	public ITCKnownSet(IntervalClock spawnPoint, IntervalClock current) {
		this.spawnPoint = spawnPoint;
		this.current = current;
	}
	
	public ITCKnownSet() {
		this(new IntervalClock(), new IntervalClock());
	}

	@Override
	public synchronized void setTask(T task) {
		if (doInit) {
			current.advanceTime();
            doInit = true;
		}
	}

	@Override
	public void fork(T task) {}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		if (knownSet == null) {
			return false;
		}
		ITCKnownSet<T> other = (ITCKnownSet<T>) knownSet;
		return ! other.spawnPoint.isEmpty() && other.spawnPoint.lessThanEquals(current);
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		ITCKnownSet<T> other = (ITCKnownSet<T>) knownSet;
		current.merge(other.current);
		current.advanceTime();
	}

	@Override
	public KnownSet<T> createChild() {
        IntervalClock otherCurrent = current.split();
        current.advanceTime();
		IntervalClock otherSpawnPoint = current.clone();
		return new ITCKnownSet<>(otherSpawnPoint, otherCurrent);
	}

	@Override
	public void clear() {
		// do nothing
	}

	@Override
	public Collection<T> asCollection() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "ITCKnownSet(spawn=" + spawnPoint + ", current=" + current + ")";
	}
}
