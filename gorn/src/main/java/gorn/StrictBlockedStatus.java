package gorn;

import pt.ul.armus.Resource;

class StrictBlockedStatus implements BlockedStatus {
	private static final OngoingWait WAITER = new OngoingWait() {
		@Override
		public boolean isRunning() {
			return true;
		}

		@Override
		public void check() {
			throw new DeadlockIdentifiedException("Strict mode enabled: unknown get!");
		}

		@Override
		public void close() {

		}
	};

	private StrictBlockedStatus() {}

	@Override
    public <T> OngoingWait waitAndUnbock(T task, SynchronizationChecker<T> checker) {
		return WAITER;
    }

    @Override
	public void setBlocked(Resource joined) {
		// nothing to do
	}

    @Override
	public void clearBlocked() {
		// nothing to do
	}

	@Override
    public void destroy() {
		// nothing to destroy
	}

	@Override
	public StrictBlockedStatus clone() {
		return this;
	}

	@Override
	public String toString() {
		return "StrictBlockedStatus";
	}

	@Override
	public void setEvent(Resource event) {
		// nothing to do
	}

	@Override
	public boolean isBlocked() {
		return false;
	}

	@Override
	public void checkDeadlock() {
		// nothing to do
	}

	@Override
	public Resource getEvent() {
		return null;
	}

	public static final StrictBlockedStatus DEFAULT = new StrictBlockedStatus();
}
