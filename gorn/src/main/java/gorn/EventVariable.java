package gorn;

import pt.ul.armus.Resource;

/**
 * Represents an event, needed for Armus.
 * 
 * @author Tiago Cogumbreiro
 * 
 */
public class EventVariable<T> implements Resource {

	private final T synch;
	private final int phase;
	private final int hashCode;

	public EventVariable(T synch) {
		this(synch, 1);
	}

	public EventVariable(T synch, final int currPhase) {
		if (synch == null || currPhase < 0) throw new IllegalArgumentException();
		this.synch = synch;
		this.phase = currPhase;
		final int prime = 31;
		int result = 1;
		result = prime * result + phase;
		result = prime * result + synch.hashCode();
		this.hashCode = result;
	}

	public T getSynch() {
		return synch;
	}

	public int getPhase() {
		return phase;
	}
	
	@Override
	public int hashCode() {
		return hashCode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventVariable<T> other;
		try {
			other = this.getClass().cast(obj);
		} catch (ClassCastException e) {
			return false;
		}
		if (phase != other.phase)
			return false;
		Object lhs = synch;
		Object rhs = other.synch;
		if (lhs == null) {
			return rhs == null;
		}
		return lhs.equals(rhs);
	}

	@Override
	public String toString() {
		return "Event[synch=" + synch + ", phase=" + phase + "]";
	}

}
