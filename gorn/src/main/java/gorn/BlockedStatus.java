package gorn;

import pt.ul.armus.Resource;

/**
 * Implements the blocked status strategy.
 */
public interface BlockedStatus extends Cloneable {
    <T> OngoingWait waitAndUnbock(T task, SynchronizationChecker<T> checker) throws DeadlockIdentifiedException;

    void setEvent(Resource resource);

    Resource getEvent();

    void setBlocked(Resource joined);

    boolean isBlocked();

    void clearBlocked();

    void destroy();

    BlockedStatus clone();

    void checkDeadlock() throws DeadlockIdentifiedException;
}
