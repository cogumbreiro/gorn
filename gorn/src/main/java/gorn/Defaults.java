package gorn;

import gorn.itc.ITCKnownSet;
import gorn.jt.JTKnownSet2;
import gorn.jt.TransitiveJoins;
import gorn.sif.*;

import java.util.EnumSet;
import java.util.concurrent.atomic.AtomicReference;

import pt.ul.armus.Armus;
import pt.ul.armus.ArmusFactory;
import pt.ul.armus.conf.ConfigurationLoader;
import pt.ul.armus.conf.MainConfiguration;

public class Defaults {

	public enum Flag {
		SIF,
		STRICT_MODE
	}

	public enum SifStrategy {
		KNOWN_GRAPH {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new BasicKnownSet<>();
            }
        },
		SNAPSHOT_SET {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new SnapKnownSet<>();
            }
        },
		SNAPSHOT_SET2 {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new SnapKnownSet2<>(false);
            }
        },
		VECTOR_CLOCK {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new VCKnownSet<>();
            }
        },
		JOIN_TREE {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new JTKnownSet2<>();
            }
        },
		INTERVAl_CLOCK {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new ITCKnownSet<>();
            }
        },
        TRANSITIVE_JOINS {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new TransitiveJoins<>();
            }
        },
        SNAPSHOT_SET2_SLOW {
            @Override
            public <T> KnownSet<T> createKnownSet() {
                return new SnapKnownSet2<>(true);
            }
        };

        /**
         * Creates an empty known-set.
         * @param <T>
         * @return
         */
        public abstract <T> KnownSet<T> createKnownSet();
    }

	private static final EnumSet<Flag> DEFAULT_FLAGS = EnumSet.of(
			Flag.SIF);
	
	private static final AtomicReference<EnumSet<Flag>> FLAGS = new AtomicReference<>(DEFAULT_FLAGS);

	private static final AtomicReference<SifStrategy> SIF_STRATEGY = new AtomicReference<>(SifStrategy.VECTOR_CLOCK);

	/**
	 * Set all flags to False.
	 */
	public static void clearFlags() {
		FLAGS.set(EnumSet.noneOf(Flag.class));
	}

	public static void resetFlags() {
		FLAGS.set(DEFAULT_FLAGS);
	}
	

	public static void setFlag(final Flag flag, final boolean status) {
		for (;;) {
			EnumSet<Flag> orig = FLAGS.get();
			EnumSet<Flag> newElement = EnumSet.copyOf(orig);
			if (status) {
				newElement.add(flag);
			} else {
				newElement.remove(flag);
			}
			if (FLAGS.compareAndSet(orig, newElement)) {
				break;
			}
		}
	}

	public static boolean isEnabled(Flag flag) {
		return FLAGS.get().contains(flag);
	}

	static Armus createArmus() {
		MainConfiguration conf = ConfigurationLoader.getDefault();
		// disable automatic deadlock avoidance/detection
		// since we will be checking for deadlock explicitly
		conf.detection.isEnabled = false;
		conf.avoidanceEnabled = false;
		return ArmusFactory.build(conf);
	}

	public static void setSifStrategy(SifStrategy strategy) {
		SIF_STRATEGY.set(strategy);
	}

	static BlockedStatus createBlockedStatus() {
		return isEnabled(Flag.STRICT_MODE) ? StrictBlockedStatus.DEFAULT :
				new ArmusBlockedStatus(createArmus());
	}

	private static BlockedContext createContext() {
	    return isEnabled(Flag.STRICT_MODE) ? new StrictContext() : new ArmusContext();
    }

	public static <T> KnownSet<T> createKnownSet() {
	    return SIF_STRATEGY.get().createKnownSet();
	}

	public static <T> TaskHandler<T> createTaskHandler() {
		return isEnabled(Flag.SIF) ? new SIFTaskHandler<T>(
				createBlockedStatus(),
				Defaults.<T> createKnownSet()) : new BareTaskHandler<T>(
				createBlockedStatus());
	}

    public static <T> Application<T> createApplication() {
	    return new Application<>(createContext(), isEnabled(Flag.SIF), SIF_STRATEGY.get());
    }

}
