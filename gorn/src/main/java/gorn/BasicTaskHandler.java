package gorn;

import pt.ul.armus.Resource;


class BasicTaskHandler<T> implements TaskHandler<T> {
	private T task = null;
	protected final BlockedStatus blocked;

	BasicTaskHandler(BlockedStatus blocked) {
		this.blocked = blocked;
	}

	@Override
	public void onSpawn(T spawned) {
	}

	/**
	 * @throws DeadlockIdentifiedException
	 *             when Armus detects a deadlock.
	 */
	@Override
	public OngoingWait beforeJoin(TaskHandler<T> joinedHandler, SynchronizationChecker<T> checker) {
		assert task != null;
		if (checker.checkBlocked(joinedHandler.getTask()) == SynchronizationResult.COMPLETE) {
			return null;
		}
		blocked.setBlocked(joinedHandler.getEvent());
		blocked.checkDeadlock();
		return null;
	}

	@Override
	public void afterJoin(TaskHandler<T> joinedTask) {
		assert joinedTask != null;
		blocked.clearBlocked();
	}

	@Override
	public void onEnd() {
		blocked.destroy();
	}

	@Override
	public TaskHandler<T> createChild() {
		return new BasicTaskHandler<>(blocked.clone());
	}

	@Override
	public String toString() {
		return "TaskHandler[task=" + task + ", blockedStatus=" + blocked + "]";
	}

	@Override
	public T getTask() {
		assert task != null : "Forgot to invoke: setTask()";
		return task;
	}

	@Override
	public void setTask(T task) {
		assert task != null;
		blocked.setEvent(new EventVariable<>(task));
		this.task = task;
	}

	@Override
	public boolean isBlocked() {
		return blocked.isBlocked();
	}

	@Override
	public Resource getEvent() {
		return blocked.getEvent();
	}
}
