package gorn;

import pt.ul.armus.Resource;

/**
 * The task-handler maintains the known-set and the blocked-status required to
 * perform deadlock avoidance.
 * 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public interface TaskHandler<T> {

	/**
	 * The first method that needs to be invoked before using this task-handler.
	 * 
	 * Sets the parent task. This information can be used to avoid checking for
	 * deadlocks. This method should <strong>not</strong> be executed
	 * concurrently with other methods. Ensure that when this value is set
	 * happens-before when this value is read (with
	 * {@link TaskHandler#getParent()}), for instance set it before spawning the
	 * task.
	 * 
	 * @param task
	 */
	void setTask(T task);

	/**
	 * Returns the task associated with this task-handler.
	 * Pre-condition: {@link TaskHandler#onStart(Object)} must have been invoked.
	 * 
	 * @return returns the value set by {@link TaskHandler#onStart(Object)}.
	 */
	T getTask();
	
	/**
	 * Returns the event that represents this task.
	 * @return
	 */
	Resource getEvent();
	
	/**
	 * Should be executed at the parent site, after spawning a task.
	 */
	void onSpawn(T child);

	/*
	 * Should be executed before a task joins with another task.
	 * @param joinedHandler
	 * @param checker
	 * @return Returning null means that there is nothing to do and is safe to join.
	 * Otherwise the task is expected wait until the task is done running. The task
	 * is also expected to check regularly whether or not there is a deadlock,
	 * as per the OngoingWait.
	 * @see OngoingWait
	 */
	OngoingWait beforeJoin(TaskHandler<T> joinedHandler, SynchronizationChecker<T> checker);

	/**
	 * Should be executed after a task joins with another task.
	 */
	void afterJoin(TaskHandler<T> joinedTask);

	/**
	 * Must be invoked to clear any associated resources to this handler.
	 */
	void onEnd();

	/**
	 * Creates a deep copy of the current handler.
	 * 
	 * @return
	 */
	TaskHandler<T> createChild();

	/**
	 * Checks if the task is blocked or not.
	 * @return
	 */
	boolean isBlocked();

}
