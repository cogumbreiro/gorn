package gorn;

import pt.ul.armus.DeadlockInfo;

/**
 * A deadlock was identified.
 * @author Tiago Cogumbreiro (cogumbreiro@users.sf.net)
 *
 */
public class DeadlockIdentifiedException extends RuntimeException {
	private static final long serialVersionUID = -681741062007033399L;
	private final DeadlockInfo deadlock;

	/**
	 *
	 * @param deadlock Cannot be null, otherwise throws a {@link NullPointerException}
	 */
	public DeadlockIdentifiedException(DeadlockInfo deadlock) throws NullPointerException {
		super(deadlock.toString());
		this.deadlock = deadlock;
	}

	public DeadlockIdentifiedException(String message) {
		super(message);
		this.deadlock = null;
	}

	public DeadlockInfo getDeadlock() {
		return deadlock;
	}
}
