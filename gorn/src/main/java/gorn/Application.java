package gorn;

public class Application<T> {
    private final BlockedContext ctx;
    private final boolean sifEnabled;
    private final Defaults.SifStrategy strategy;

    public Application(BlockedContext ctx, boolean sifEnabled, Defaults.SifStrategy strategy) {
        this.ctx = ctx;
        this.sifEnabled = sifEnabled;
        this.strategy = strategy;
    }

    public TaskHandler<T> createTaskHandler() {
        return sifEnabled ? new SIFTaskHandler<T>(
                ctx.createBlockedStatus(),
                strategy.<T>createKnownSet()) :
                new BareTaskHandler<T>(ctx.createBlockedStatus());
    }
    public void start() {
        ctx.start();
    }
    public void stop() {
        ctx.stop();
    }
}
