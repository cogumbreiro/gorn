package gorn;

public interface BlockedContext {
    BlockedStatus createBlockedStatus();
    void start();
    void stop();
}
