package gorn;

import pt.ul.armus.Armus;
import pt.ul.armus.ArmusFactory;
import pt.ul.armus.conf.ConfigurationLoader;
import pt.ul.armus.conf.MainConfiguration;

public class ArmusContext implements BlockedContext {
    private final Armus armus;

    public ArmusContext() {
        MainConfiguration conf = ConfigurationLoader.getDefault();
        // disable automatic deadlock avoidance/detection
        // since we will be checking for deadlock explicitly
        conf.detection.isEnabled = false;
        conf.avoidanceEnabled = false;
        this.armus = ArmusFactory.build(conf);
    }


    @Override
    public BlockedStatus createBlockedStatus() {
        return new ArmusBlockedStatus(armus);
    }

    @Override
    public void stop() {
        armus.stop();
    }

    @Override
    public void start() {
        armus.start();
    }
}
