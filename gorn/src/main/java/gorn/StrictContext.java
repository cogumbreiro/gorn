package gorn;

public class StrictContext implements BlockedContext {

    @Override
    public BlockedStatus createBlockedStatus() {
        return StrictBlockedStatus.DEFAULT;
    }

    @Override
    public void start() {
        // nothing to do
    }

    @Override
    public void stop() {
        // nothing to do
    }
}
