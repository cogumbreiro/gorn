package gorn;

public interface SynchronizationChecker<T> {
	/**
	 * 
	 * @param synch
	 * @return
	 */
	SynchronizationResult checkBlocked(T synch);
}
