package gorn;

import pt.ul.armus.Armus;
import pt.ul.armus.DeadlockInfo;
import pt.ul.armus.Resource;
import pt.ul.armus.deadlockresolver.DeadlockFoundException;
import pt.ul.armus.edgebuffer.EdgeSetFactory;
import pt.ul.armus.edgebuffer.TaskHandle;

class ArmusBlockedStatus implements BlockedStatus {
	private final Armus verifier;
	private final TaskHandle buffer;
	private Resource event = null;

	private ArmusBlockedStatus(Armus verifier, TaskHandle buffer) {
		this.verifier = verifier;
		this.buffer = buffer;
	}

	ArmusBlockedStatus(Armus verifier) {
		this.verifier = verifier;
		this.buffer = verifier.getBuffer().createHandle();
	}

	private class Waiting<T> implements OngoingWait {
		private final T task;
		private final SynchronizationChecker<T> checker;
		public Waiting(T task, SynchronizationChecker<T> checker) {
			this.task = task;
			this.checker = checker;
		}
		@Override
		public boolean isRunning() {
			return checker.checkBlocked(task) != SynchronizationResult.COMPLETE;
		}

		@Override
		public void check() {
			checkDeadlock();
		}

		@Override
		public void close() {
			clearBlocked();
		}
	}

	@Override
    public <T> OngoingWait waitAndUnbock(T task, SynchronizationChecker<T> checker) {
		return new Waiting(task, checker);
    }

    @Override
	public void setBlocked(Resource joined) {
		assert !buffer.isBlocked() : "last join forgot to invoke afterJoin()";
		assert event != null : "forgot to invoke setTask()";
		assert joined != null;
		try {
			buffer.setBlocked(EdgeSetFactory
					.requestOneAllocateOne(joined, event));
		} catch (DeadlockFoundException e) {
		    // Deadlocks are checked explicitly, so this exception should never be thrown
			throw new IllegalStateException("Should not happen!");
		}
	}

    @Override
	public void clearBlocked() {
	    if (isBlocked()) {
            buffer.clearBlocked();
        }
	}

	@Override
    public void destroy() {
		assert !buffer.isBlocked() : "forgot to invoke afterJoin()";
		verifier.getBuffer().destroy(buffer);
	}

	@Override
	public ArmusBlockedStatus clone() {
		return new ArmusBlockedStatus(verifier, verifier.getBuffer()
				.createHandle());
	}

	@Override
	public String toString() {
		return "BlockedStatus[event=" + event + ", blockedStatus="
				+ buffer.isBlocked() + "]";
	}

	public Resource getEvent() {
		assert event != null : "Forgot to invoke: setEvent()";
		return event;
	}

	@Override
	public void setEvent(Resource event) {
		assert event != null;
		this.event = event;
	}

	@Override
	public boolean isBlocked() {
		return buffer.isBlocked();
	}

	@Override
	public void checkDeadlock() throws DeadlockIdentifiedException {
		DeadlockInfo deadlock = verifier.checkDeadlock();
		if (deadlock != null) {
			// When this method throws an exception, we do not expect the
			// user
			// of this library
			throw new DeadlockIdentifiedException(deadlock);
		}
	}
}
