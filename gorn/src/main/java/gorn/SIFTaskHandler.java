package gorn;

import gorn.sif.KnownSet;
import pt.ul.armus.Resource;

class SIFTaskHandler<T> extends BasicTaskHandler<T> {
	private final KnownSet<T> knownSet;

	SIFTaskHandler(BlockedStatus blocked, KnownSet<T> knownSet) {
		super(blocked);
		this.knownSet = knownSet;
	}

	@Override
	public void onSpawn(T spawned) {
		knownSet.fork(spawned);
	}

	@Override
	public void setTask(T task) {
		super.setTask(task);
		knownSet.setTask(task);
	}
	
	/**
	 * @throws DeadlockIdentifiedException
	 *             when Armus detects a deadlock.
	 */
	@Override
	public OngoingWait beforeJoin(TaskHandler<T> joinedHandler, SynchronizationChecker<T> checker) {
		assert joinedHandler != null;
		T joinedTask = joinedHandler.getTask();
		assert joinedTask != null;
		if (checker.checkBlocked(joinedHandler.getTask()) == SynchronizationResult.COMPLETE) {
			return null;
		}
		Resource event = joinedHandler.getEvent();
		SIFTaskHandler<T> other = (SIFTaskHandler<T>) joinedHandler;
		blocked.setBlocked(event);
		return knownSet.canJoin(joinedTask, other.knownSet)
				? null
				: blocked.waitAndUnbock(joinedTask, checker);
	}

	@Override
	public void afterJoin(TaskHandler<T> joinedTask) {
		blocked.clearBlocked();
		SIFTaskHandler<T> other = (SIFTaskHandler<T>) joinedTask;
		knownSet.join(other.getTask(), other.knownSet);
	}

	@Override
	public TaskHandler<T> createChild() {
		return new SIFTaskHandler<>(blocked.clone(), knownSet.createChild());
	}

	@Override
	public String toString() {
		return "TaskHandler[task=" + getTask() + ", knownSet=" + knownSet + ", blockedStatus="
				+ blocked + "]";
	}
}
