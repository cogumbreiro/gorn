package gorn.sif;

import java.util.Collection;

import gorn.vc.VectorClock;

public class VCKnownSet<T> implements KnownSet<T> {
	private volatile int id;
	private final VectorClock spawnPoint;
	private final VectorClock current;

	public VCKnownSet(int id, VectorClock spawnPoint, VectorClock current) {
		this.id = id;
		this.spawnPoint = spawnPoint;
		this.current = current;
	}
	
	public VCKnownSet() {
		this(0, new VectorClock(), new VectorClock());
	}

	@Override
	public synchronized void setTask(T task) {
		int newId = task.hashCode();
		if (newId != id) {
			id = newId;
			current.advanceTime(newId);
		}
	}

	@Override
	public void fork(T task) {
		current.advanceTime(id);
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		if (knownSet == null) {
			return false;
		}
		VCKnownSet<T> other = (VCKnownSet<T>) knownSet;
		return ! other.spawnPoint.isEmpty() && other.spawnPoint.happensBeforeReflexive(current);
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		VCKnownSet<T> other = (VCKnownSet<T>) knownSet;
		current.merge(other.current);
		current.advanceTime(id);
	}

	@Override
	public KnownSet<T> createChild() {
		VectorClock otherSpawnPoint = current.clone();
		otherSpawnPoint.advanceTime(id);
		VectorClock otherCurrent = this.current.clone();
		return new VCKnownSet<>(id, otherSpawnPoint, otherCurrent);
	}

	@Override
	public void clear() {
		// do nothing
	}

	@Override
	public Collection<T> asCollection() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return "ITCKnownSet(id=" + id + ", spawn=" + spawnPoint + ", current=" + current + ")";
	}
}
