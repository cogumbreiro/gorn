package gorn.sif;

import gorn.sif.snapshotset.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The basic known-set uses a global and immutable Knowledge Graph to test set
 * membership.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class SnapKnownSet<T> implements KnownSet<T> {
	private Tree<Container<T>> tree;
	private SnapshotSet<T> set = null;

	public SnapKnownSet() {
		this.tree = null;
	}

	protected SnapKnownSet(Tree<Container<T>> graph) {
		this.tree = graph;
	}

	@Override
	public void fork(T task) {
		// set creation is lazy
		if (set == null) {
			set = new SnapshotSetBucket<>();
		}
		set.add(task);
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		SnapKnownSet<T> other = (SnapKnownSet<T>) knownSet;
		if (set != null && other.set != null && set.equals(other.set)) {
			if (other.tree != null) {
				tree = tree != null ? new Tree<>(null, tree,
						other.tree) : other.tree;
			}
			return;
		}
		if (other.set != null || other.tree != null) {
			tree = new Tree<>(other.set, tree, other.tree);
		}
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		return (set != null && set.contains(task))
				|| (tree != null && SnapGraph.<T>contains(tree, task));
	}

	@Override
	public Collection<T> asCollection() {
		List<T> result = new ArrayList<>();
		if (set != null) {
			for (T elem : set) {
				result.add(elem);
			}
		}
		if (tree != null) {
			result.addAll(SnapGraph.asCollection(tree));
		}
		return result;
	}

	@Override
	public SnapKnownSet<T> createChild() {
		return set == null ? new SnapKnownSet<T>(tree) : new SnapKnownSet<T>(
				new Tree<>(set.createSnapshot(), tree, null));
	}

	@Override
	public void clear() {
		set = null;
		tree = null;
	}

	@Override
	public void setTask(T task) {
	}
}
