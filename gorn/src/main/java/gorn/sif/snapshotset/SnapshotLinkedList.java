package gorn.sif.snapshotset;

import gorn.util.Link;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * A read-only linked list where the values have a timestep associated with it.
 * The list can then be used to query if an element is in the list with a given
 * timestep.
 * 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class SnapshotLinkedList<T> implements SnapshotList<T> {
	private Link<Snapshot<T>> head = null;
	private Link<Snapshot<T>> tail = null;
	private int size;

	@Override
	public void add(Snapshot<T> snap) {
		Link<Snapshot<T>> link = new Link<>(snap);
		if (head == null) {
			tail = head = link;
		} else {
			tail.next = link;
			tail = tail.next;
		}
		size++;
	}

	@Override
	public boolean contains(T value) {
		if (head == null) {
			return false;
		}
		for (Snapshot<T> snap : head) {
			if (snap.contains(value)) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gorn.sif.SnapshotList#contains(int, T)
	 */
	@Override
	public boolean contains(int timestep, T value) {
		if (head == null) {
			return false;
		}
		for (Snapshot<T> snap : head) {
			if (!snap.isVisibleAt(timestep)) {
				// we assume the elements are ordered
				// so, further elements will all be invisible
				return false;
			}
			if (snap.contains(value)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<Snapshot<T>> iterator() {
		return head == null ? Collections.<Snapshot<T>>emptyIterator() : head.iterator();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String toString() {
		ArrayList<Snapshot<T>> result = new ArrayList<>();
		for (Snapshot<T> entry : this) {
			result.add(entry);
		}
		return result.toString();
	}
}