package gorn.sif.snapshotset;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import com.carrotsearch.hppc.BitMixer;


/**
 * Implements the hashing algorithm in a single array.
 * 
 * @author Tiago Cogumbreiro
 *
 */
class HashArray<T> implements Iterable<T> {
	public final Object[] elems;

	public HashArray(int size) {
		// Approximate the size of the array to the nearest power of two
		elems = new Object[1 << (byte) Math.ceil(Math.log(size)/Math.log(2))];
	}

	/**
	 * Obtains the index within the backing array, given a hashcode.
	 * @param hashCode
	 * @return
	 */
	public int indexOf(int hashCode) {
		return hashOf(hashCode) & (elems.length - 1);
	}
	
	/**
	 * Returns the object at this position, given a hashcode.
	 * @param hashCode
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T get(int hashCode) {
		return (T) elems[indexOf(hashCode)];
	}
	
	/**
	 * Stores an element in the array, given a hashcode of the key.
	 * @param hashCode
	 * @param obj
	 */
	public void put(int hashCode, T obj) {
		elems[indexOf(hashCode)] = obj;
	}
	
	/**
	 * Hash the key.
	 * @param h
	 * @return
	 */
	private static int hashOf(int h) {
		return BitMixer.mix(h);
	}
	
	/**
	 * 
	 * @return Returns the size of the backing array.
	 */
	public int length() {
		return elems.length;
	}

	/**
	 * Iterates over all non-null elements of the array. 
	 *
	 * @param <T>
	 */
	private static class Iter<T> implements Iterator<T> {
		private int index = 0;
		private final Object elems[];

		public Iter(Object[] elems) {
			this.elems = elems;
			forward();
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean hasNext() {
			return index < elems.length && elems[index] != null;
		}

		private void forward() {
			int i = index;
			for (; i < elems.length && elems[i] == null; i++) {
				// nothing to do
			}
			index = i;
		}

		@Override
		public T next() {
			T result = (T) elems[index];
			index++;
			forward();
			return result;
		}
		
		@Override
		public void remove() {
	        throw new UnsupportedOperationException("remove");
	    }

		@Override
		public String toString() {
			return "ArrayIter[cursor=" + index + ", array="+ Arrays.toString(elems) + "]";
		}
	}
	
	@Override
	public Iterator<T> iterator() {
		return new Iter<>(elems);
	}

	private Collection<T> asCollection() {
		ArrayList<T> result = new ArrayList<>();
		for (Object obj : elems) {
			if (obj != null) {
				result.add((T) obj);
			}
		}
		return result;
	}

	@Override
	public String toString() {
		return asCollection().toString();
	}
}
