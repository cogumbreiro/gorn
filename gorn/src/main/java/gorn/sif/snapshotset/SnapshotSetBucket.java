package gorn.sif.snapshotset;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Note that this set will admit repeated elements.
 * 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class SnapshotSetBucket<T> implements SnapshotSet<T> {
	private HashArray<SnapshotList<T>> data;
	private int size;
	private float loadFactor = 0.80f;
	
	public SnapshotSetBucket(int allocationSize) {
		data = new HashArray<>(allocationSize);
		size = 0;
	}
	
	/**
	 * By default the hashset has a capacity of 16 elements.
	 */
	public SnapshotSetBucket() {
		this(16);
	}
	
	@Override
	public void add(T value) {
		doPut(data, new Snapshot<>(size, value));
		size++;
		rehashIfNeeded();
	}

	private static <T> void doPut(HashArray<SnapshotList<T>> data, Snapshot<T> snap) {
		int hashCode = snap.value.hashCode();
		SnapshotList<T> list = data.get(hashCode);
		if (list == null) {
			list = new SnapshotArrayList<>();
			data.put(hashCode, list);
		}
		list.add(snap);
	}
	
	@Override
	public boolean contains(T value) {
		SnapshotList<T> entry = data.get(value.hashCode());
		return entry != null && entry.contains(value);
	}

	private static <T> boolean contains(HashArray<SnapshotList<T>> data, int timestep, T value) {
		SnapshotList<T> entry = data.get(value.hashCode());
		return entry != null && entry.contains(timestep, value);
	}
	
	private void rehashIfNeeded() {
		if (size <= data.length() * loadFactor) {
			return;
		}
		// create the new array
		HashArray<SnapshotList<T>> newData = new HashArray<>(size << 1);
		// copy the objects to the new array
		for (Object obj : data.elems) {
			if (obj == null) {
				// skip null elements
				continue;
			}
			// iterate over each bucket and put it in the array
			@SuppressWarnings("unchecked")
			SnapshotList<T> entry = ((SnapshotList<T>) obj);
			for (Snapshot<T> snap :  entry) {
				doPut(newData, snap);
			}
		}
		// updates the backing array
		data = newData;
	}
	
	@Override
	public Container<T> createSnapshot() {
		return new View(size);
	}
	
	/**
	 * Returns the number of elements in the snapshot.
	 * @return
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Returns the size of the backing array.
	 * @return
	 */
	public int capacity() {
		return data.length();
	}

	@Override
	public Iterator<T> iterator() {
		return new Iter<>(data.iterator(), -1);
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof Container && hashCode() == obj.hashCode();
	}
	
	@Override
	public int getTimestep() {
		return size;
	}

	@Override
	public String toString() {
		return data.toString();
	}

	private final class View implements Container<T> {
		private final int timestep;

		public View(int timestep) {
			this.timestep = timestep;
		}

		@Override
		public Iterator<T> iterator() {
			// uses the data field of the parent class
			return new Iter<>(data.iterator(), timestep);
		}

		@Override
		public boolean contains(T key) {
			// uses the data field of the parent class
			return SnapshotSetBucket.contains(data, timestep, key);
		}

		@Override
		public int getTimestep() {
			return timestep;
		}

		@Override
		public int hashCode() {
			return SnapshotSetBucket.this.hashCode();
		}
	}

	private static final class Iter<T> implements Iterator<T> {
		private final Iterator<SnapshotList<T>> buckets;
		private Iterator<T> current = null;
		private final int timestep;
		
		public Iter(Iterator<SnapshotList<T>> buckets, int timestep) {
			this.buckets = buckets;
			this.timestep = timestep;
		}

		@Override
		public boolean hasNext() {
			if (current != null && current.hasNext()) {
				return true;
			}
			while (buckets.hasNext()) {
				SnapshotList<T> next = buckets.next();
				// Copy the contents of the bucket that are visible
				ArrayList<T> entries = new ArrayList<>(next.size());
				for (Snapshot<T> entry : next) {
					if (entry.isVisibleAt(timestep)) {
						entries.add(entry.value);
					}
				}
				current = entries.iterator();
				if (current.hasNext()) {
					return true;
				}
			}
			return false;
		}
		
		@Override
		public T next() {
			return current.next();
		}
		
		@Override
		public void remove() {
	        throw new UnsupportedOperationException("remove");
	    }
	}
}
