package gorn.sif.snapshotset;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * A read-only array list where the values have a timestep associated with it.
 * The list can then be used to query if an element is in the list with a given
 * timestep.
 * 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class SnapshotArrayList<T> implements SnapshotList<T> {
	private Snapshot<T>[] buffer;
	private int offset;

	@SuppressWarnings("unchecked")
	public SnapshotArrayList(int initialSize) {
		this.buffer = new Snapshot[initialSize];
	}

	public SnapshotArrayList() {
		this(8);
	}

	@Override
	public int size() {
		return offset;
	}

	@Override
	public void add(Snapshot<T> snap) {
		buffer[offset] = snap;
		offset++;
		if (offset == buffer.length) {
            buffer = Arrays.copyOf(buffer, buffer.length << 1);
		}
	}

	@Override
	public boolean contains(T value) {
	    // ensure we have we copy the location of the buffer
        final Snapshot<T>[] snaps = buffer;
        // now use this buffer to range over its elements
		for (Snapshot<T> snap : snaps) {
			if (snap == null) {
				break;
			}
			if (snap.contains(value)) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see gorn.sif.SnapshotList#contains(int, T)
	 */
	@Override
	public boolean contains(int timestep, T value) {
        // ensure we have we copy the location of the buffer
        final Snapshot<T>[] snaps = buffer;
        // now use this buffer to range over its elements
        for (Snapshot<T> snap : snaps) {
			if (snap == null) {
				break;
			}
			if (!snap.isVisibleAt(timestep)) {
				// we assume the elements are ordered
				// so, further elements will all be invisible
				return false;
			}
			if (snap.contains(value)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Iterator<Snapshot<T>> iterator() {
		return new ArrayIter<>(buffer);
	}

	@Override
	public String toString() {
		ArrayList<Snapshot<T>> result = new ArrayList<>();
		for (Snapshot<T> entry : buffer) {
			if (entry != null) {
				result.add(entry);
			}
		}
		return result.toString();
	}

	private static class ArrayIter<T> implements Iterator<Snapshot<T>> {
		private final Snapshot<T> data[];
		private int cursor;

		public ArrayIter(Snapshot<T> data[]) {
			this.data = data;
		}

		@Override
		public boolean hasNext() {
			return cursor < data.length && data[cursor] != null;
		}

		@Override
		public Snapshot<T> next() {
			Snapshot<T> curr = data[cursor];
			cursor++;
			return curr;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String toString() {
			return "ArrayIter[cursor=" + cursor + ", array="+ Arrays.toString(data) + "]";
		}
	}

}