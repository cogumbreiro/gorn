package gorn.sif.snapshotset;


public interface Container<T> extends Iterable<T> {
	boolean contains(T key);
	int getTimestep();
}
