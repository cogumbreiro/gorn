package gorn.sif.snapshotset;

public interface SnapshotSet<T> extends Container<T> {

	/**
	 * Adds the element to the set.
	 * @param value
	 */
	void add(T value);

	Container<T> createSnapshot();
}