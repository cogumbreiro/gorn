package gorn.sif.snapshotset;

import java.util.Iterator;

public interface SnapshotList<T> extends Iterable<Snapshot<T>> {

	/**
	 * Creates a new link of the list, pointing to a previous list.
	 * @param entry
	 */
	void add(Snapshot<T> entry);

	/**
	 * Checks if the element is in the linked list (using the equals method).
	 * This method call ignores timesteps.
	 * @param value
	 * @return
	 */
	boolean contains(T value);

	/**
	 * Checks if an element within the given timestep is in the list.
	 * @param timestep Any element with a timestep greater than this will be ignored.
	 * @param value The value we are looking for.
	 * @return If the element is in the list.
	 */
	boolean contains(int timestep, T value);

	/**
	 * Counts the number of elements in the list.
	 * @return
	 */
	int size();
}