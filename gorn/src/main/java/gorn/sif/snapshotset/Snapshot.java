package gorn.sif.snapshotset;

/**
 * A snapshot holds a value that is visible up to a certain timestep.
 * 
 * @author Tiago Cogumbreiro
 *
 * @param <T>
 */
public class Snapshot<T> {
	public final int timestep;
	public final T value;

	/**
	 * Creates a new link of the list, pointing to a previous list.
	 * 
	 * @param timestep
	 * @param value
	 */
	public Snapshot(int timestep, T value) {
		this.timestep = timestep;
		this.value = value;
	}

	/**
	 * Checks if the element is in the linked list (using the equals method).
	 * This method call ignores timesteps.
	 * 
	 * @param value
	 * @return
	 */
	public boolean contains(T value) {
		return (value == null && this.value == null)
				|| (value != null && value.equals(this.value));
	}

	/**
	 * Checks if this entry is visible at a given timestep.
	 * 
	 * @param timestep Negative means at any time.
	 * @return
	 */
	public boolean isVisibleAt(int timestep) {
		return timestep < 0 || this.timestep <= timestep;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + timestep;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Snapshot other = (Snapshot) obj;
		if (timestep != other.timestep)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return value + "@" + timestep;
	}
}