package gorn.sif.snapshotset;

/**
 * @author Tiago Cogumbreiro
 */
public class Tree<T> {
    public T value;
    public Tree<T> left;
    public Tree<T> right;

    public Tree() {
    }

    public Tree(T value) {
        this.value = value;
    }
    public Tree(T value, Tree<T> left, Tree<T> right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
    public Tree(Tree<T> left, Tree<T> right) {
        this.left = left;
        this.right = right;
    }

    public boolean isEmpty() {
        return value == null && left == null && right == null;
    }
}
