package gorn.sif.snapshotset;

import java.util.ArrayList;
import java.util.Collection;

import com.carrotsearch.hppc.ObjectStack;

/**
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class SnapGraph {

	/**
	 * Checks if the element is in the set.
	 * 
	 * @param elem
	 * @return
	 */
	public static <T> boolean contains(Tree<Container<T>> curr, T elem) {
		if (elem == null) {
			throw new IllegalArgumentException();
		}
		if (curr.value != null && curr.value.contains(elem)) {
			return true;
		}
		ObjectStack<Tree<Container<T>>> toProcess = new ObjectStack<>();
		toProcess.push(curr);
		while (toProcess.size() > 0) {
			Tree<Container<T>> g = toProcess.pop();
			if (g.value != null && g.value.contains(elem)) {
				return true;
			}
			SnapGraph.<Tree<Container<T>>>push(toProcess, g.right);
			SnapGraph.<Tree<Container<T>>>push(toProcess, g.left);
		}
		return false;
	}

	private static <T> void push(ObjectStack<T> toProcess,  T object) {
		if (object != null ) {
			toProcess.add(object);
		}
	}
	
	public static <T> Collection<T> asCollection(Tree<Container<T>> curr) {
		ArrayList<T> result = new ArrayList<>();
		ObjectStack<Tree<Container<T>>> toProcess = new ObjectStack<>();
		toProcess.push(curr);
		while (toProcess.size() > 0) {
			Tree<Container<T>> g = toProcess.pop();
			if (g.value != null) {
				for (T cursor : g.value) {
					result.add(cursor);
				}
			}
			SnapGraph.<Tree<Container<T>>>push(toProcess, g.right);
			SnapGraph.<Tree<Container<T>>>push(toProcess, g.left);
		}
		return result;
	}
}
