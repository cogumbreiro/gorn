package gorn.sif.snapshotset;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Util {
	public static <T> List<T> asList(Iterable<T> iter) {
		return asList(iter.iterator());
	}
	
	public static <T> List<T> asList(Iterator<T> iter) {
		ArrayList<T> result = new ArrayList<T>();
		while(iter.hasNext()) {
			result.add(iter.next());
		}
		return result;
	}

}
