package gorn.sif.knowngraph;

import java.util.ArrayList;
import java.util.Collection;

import com.carrotsearch.hppc.ObjectStack;

/**
 * 
 * @author Tiago Cogumbreiro, Rishi Surendan
 *
 */
public class KnowledgeGraph<T> {
	private final T elem;
	private final KnowledgeGraph<T> left;
	private final KnowledgeGraph<T> right;

	/**
	 * Creates an empty knowledge graph.
	 */
	public KnowledgeGraph() {
		elem = null;
		left = null;
		right = null;
	}

	/**
	 * Add one elemnt
	 * 
	 * @param elem
	 * @param left
	 */
	private KnowledgeGraph(T elem, KnowledgeGraph<T> left) {
		assert elem != null;
		assert left != null;
		this.elem = elem;
		this.left = left;
		this.right = null;
	}

	/**
	 * Union of two nodes.
	 * 
	 * @param left
	 * @param right
	 */
	private KnowledgeGraph(KnowledgeGraph<T> left, KnowledgeGraph<T> right) {
		assert left != null && right != null;
		this.elem = null;
		this.left = left;
		this.right = right;
	}

	/**
	 * Adds a new element to the set.
	 * 
	 * @param task
	 * @return
	 */
	public KnowledgeGraph<T> add(T task) {
		return new KnowledgeGraph<T>(task, this);
	}

	/**
	 * Only add if the other graph is not empty.
	 * @param k
	 * @return
	 */
	public KnowledgeGraph<T> union(KnowledgeGraph<T> k) {
		return k.isEmpty() ? this : new KnowledgeGraph<>(k, this);
	}

	/**
	 * Checks if the element is in the set.
	 * 
	 * @param elem
	 * @return
	 */
	public boolean contains(T elem) {
		if (elem == null) {
			throw new IllegalArgumentException();
		}
		if (elem.equals(this.elem)) {
			return true;
		}
		ObjectStack<KnowledgeGraph<T>> toProcess = new ObjectStack<>();
		toProcess.push(this);
		while (toProcess.size() > 0) {
			KnowledgeGraph<T> g = toProcess.pop();
			if (elem.equals(g.elem)) {
				return true;
			}
			if (g.left != null) toProcess.push(g.left);
			if (g.right != null) toProcess.push(g.right);
		}
		return false;
	}
	
	public static <T> Collection<T> asCollection(KnowledgeGraph<T> graph) {
		ArrayList<T> result = new ArrayList<>();
		ArrayList<KnowledgeGraph<T>> toProcess = new ArrayList<>();
		toProcess.add(graph);
		while (toProcess.size() > 0) {
			ArrayList<KnowledgeGraph<T>> children = new ArrayList<>();
			for (KnowledgeGraph<T> known : toProcess) {
				if (known == null) {
					continue;
				}
				if (known.left != null) {
					children.add(known.left);
				}
				if (known.right !=  null) {
					children.add(known.right);
				}
				if (known.elem != null) {
					result.add(known.elem);
				}
			}
			toProcess.clear();
			toProcess.addAll(children);
		}
		return result;
	}
	
	public boolean isEmpty() {
		return elem == null && right == null && left == null;
	}
}
