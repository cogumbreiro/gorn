package gorn.sif;

import java.util.Collection;

public interface KnownSet<T> {
	void setTask(T task);
	
	/**
	 * Spawns a new task, adding it to the current set, returns the known-set of the task.
	 * @param task The new task being spawned.
	 * @return The known-set of the child task.
	 */
	void fork(T task);

	/**
	 * Checks wether or not a task is safe to join.
	 */
	boolean canJoin(T task, KnownSet<T> knownSet);

	/**
	 * Updates the current known-set as the union of the current known-set and the given known set.
	 */
	void join(T task, KnownSet<T> knownSet);

	KnownSet<T> createChild();
	
	void clear();
	
	/**
	 * Returns all tasks in the set as a collection. 
	 * @return
	 */
	Collection<T> asCollection();
}