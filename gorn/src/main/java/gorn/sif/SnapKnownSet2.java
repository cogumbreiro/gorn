package gorn.sif;

import com.carrotsearch.hppc.IntHashSet;
import com.carrotsearch.hppc.ObjectStack;
import gorn.sif.snapshotset.Container;
import gorn.util.Link;
import gorn.sif.snapshotset.SnapshotSet;
import gorn.sif.snapshotset.SnapshotSetBucket;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * The basic known-set uses a global and immutable Knowledge Graph to test set
 * membership.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class SnapKnownSet2<T> implements KnownSet<T> {
    private final AtomicInteger idGen;
    private final int id;
	private SnapshotSet<T> set = null;
    private Link<Container<T>> parents = null;
    private Link<SnapKnownSet2<T>> joins = null;

    private boolean isEmpty() {
        return set == null && joins == null && parents == null;
    }

	public SnapKnownSet2() {
        this(false);
    }

    public SnapKnownSet2(boolean trackVisited) {
        idGen = trackVisited ? new AtomicInteger() : null;
        id = idGen != null ? idGen.getAndIncrement() : 0;
    }

    protected SnapKnownSet2(AtomicInteger idGen, Link<Container<T>> parents, Link<SnapKnownSet2<T>> joins) {
        this.idGen = idGen;
        id = idGen != null ? idGen.getAndIncrement() : 0;
		this.parents = parents;
        this.joins = joins;
	}

	@Override
	public void fork(T task) {
		// set creation is lazy
		if (set == null) {
			set = new SnapshotSetBucket<>();
		}
		set.add(task);
	}

	private Link<Container<T>> copyParents() {
        return set == null ? parents : new Link<>(set.createSnapshot(), parents);
    }

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		SnapKnownSet2<T> other = (SnapKnownSet2<T>) knownSet;
        if (! other.isEmpty()) {
            joins = new Link<>(other, joins);
        }
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
        return canJoin(this, task, idGen != null);
	}

	private static <T> boolean canJoin(SnapKnownSet2<T> first, T task, boolean checkVisited) {
        ObjectStack<SnapKnownSet2<T>> toProcess = new ObjectStack<>();
        toProcess.push(first);
        IntHashSet visited = new IntHashSet();
        visited.add(first.id);
        while (toProcess.size() > 0) {
            SnapKnownSet2<T> k = toProcess.pop();
            if (k.set != null && k.set.contains(task)) {
                return true;
            }
            for (Container<T> entry : Link.iterate(k.parents)) {
                if (entry.contains(task)) {
                    return true;
                }
            }
            for (SnapKnownSet2<T> entry : Link.iterate(k.joins)) {
                if (!checkVisited || ! visited.contains(entry.id)) {
                    if (checkVisited) {
                        visited.add(entry.id);
                    }
                    toProcess.add(entry);
                }
            }
        }
        return false;
    }



	@Override
	public Collection<T> asCollection() {
        ObjectStack<SnapKnownSet2<T>> toProcess = new ObjectStack<>();
        toProcess.push(this);
        return asCollection(toProcess);
	}

	private static <T> Collection<T> asCollection(ObjectStack<SnapKnownSet2<T>> toProcess) {
		TreeSet<T> result = new TreeSet<>();
		while (toProcess.size() > 0) {
			SnapKnownSet2<T> k = toProcess.pop();
			if (k.set != null) {
				for (T elem : k.set) {
					result.add(elem);
				}
			}
			for (Container<T> container : Link.iterate(k.parents)) {
				for (T entry : container) {
					result.add(entry);
				}
			}
			for (SnapKnownSet2<T> entry : Link.iterate(k.joins)) {
				toProcess.add(entry);
			}
		}
		return result;
	}

	@Override
	public SnapKnownSet2<T> createChild() {
		return new SnapKnownSet2<T>(idGen, copyParents(), joins);
	}

	@Override
	public void clear() {
		set = null;
		joins = null;
        parents = null;
	}

	@Override
	public void setTask(T task) {
	}
}
