package gorn.sif;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The basic known-set uses a global and immutable Knowledge Graph to test set
 * membership.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class HistoryKnownSet<T> implements KnownSet<T> {
	private Set<T> history;

	public HistoryKnownSet() {
		this.history = new HashSet<>();
	}

	protected HistoryKnownSet(Set<T> graph) {
		this.history = graph;
	}

	@Override
	public void fork(T task) {
		history.add(task);
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		if (!(knownSet instanceof HistoryKnownSet)) {
			throw new IllegalArgumentException();
		}
		HistoryKnownSet<T> other = (HistoryKnownSet<T>) knownSet;
		history.addAll(other.history);
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		return history.contains(task);
	}

	@Override
	public Collection<T> asCollection() {
		return history;
	}
	
	@Override
	public HistoryKnownSet<T> createChild() {
		return new HistoryKnownSet<T>(new HashSet<>(history));
	}

	@Override
	public void clear() {
		history.clear();
	}

	@Override
	public void setTask(T task) {
	}
	
	@Override
	public String toString() {
		return asCollection().toString();
	}
}
