package gorn.sif;

import gorn.sif.knowngraph.KnowledgeGraph;

import java.util.Collection;

/**
 * The basic known-set uses a global and immutable Knowledge Graph to test set
 * membership.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class BasicKnownSet<T> implements KnownSet<T> {
	private KnowledgeGraph<T> graph;

	public BasicKnownSet() {
		this.graph = new KnowledgeGraph<>();
	}

	protected BasicKnownSet(KnowledgeGraph<T> graph) {
		this.graph = graph;
	}

	@Override
	public void fork(T task) {
		graph = graph.add(task);
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		if (!(knownSet instanceof BasicKnownSet)) {
			throw new IllegalArgumentException();
		}
		BasicKnownSet<T> other = (BasicKnownSet<T>) knownSet;
		graph = graph.union(other.graph);
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		return graph.contains(task);
	}

	@Override
	public Collection<T> asCollection() {
		return KnowledgeGraph.asCollection(graph);
	}
	
	@Override
	public BasicKnownSet<T> createChild() {
		return new BasicKnownSet<T>(graph);
	}

	@Override
	public void clear() {
		graph = new KnowledgeGraph<>();
	}

	@Override
	public void setTask(T task) {
	}
	
	@Override
	public String toString() {
		return asCollection().toString();
	}
}
