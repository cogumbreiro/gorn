package gorn;

class BareTaskHandler<T> extends BasicTaskHandler<T> {
	public BareTaskHandler(BlockedStatus blocked) {
		super(blocked);
	}

	@Override
	public void onSpawn(T spawned) {
	}

	/**
	 * @throws DeadlockIdentifiedException
	 *             when Armus detects a deadlock.
	 */
	@Override
	public OngoingWait beforeJoin(TaskHandler<T> joinedHandler, SynchronizationChecker<T> checker) {
		assert joinedHandler != null;
		if (checker.checkBlocked(joinedHandler.getTask()) == SynchronizationResult.COMPLETE) {
			return null;
		}
		blocked.setBlocked(joinedHandler.getEvent());
		blocked.checkDeadlock();
		return null;
	}

	@Override
	public void afterJoin(TaskHandler<T> joinedTask) {
		assert joinedTask != null;
		blocked.clearBlocked();
	}

	@Override
	public TaskHandler<T> createChild() {
		return new BareTaskHandler<>(blocked.clone());
	}

	@Override
	public String toString() {
		return "TaskHandler[task=" + getTask() + ", blockedStatus="
				+ blocked + "]";
	}
}
