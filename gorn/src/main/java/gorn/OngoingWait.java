package gorn;

/**
 * Represents an ongoing wait-operation, where the user
 * of this class must invoke isRunning before invoking check.
 * <pre>
 *     try (Waiter x = factory()) {
 *          while(x.isRunning()) {
 *              x.check();
 *          }
 *     }
 * </pre>
 */
public interface OngoingWait extends AutoCloseable {
	/**
	 *
	 * @return If the operation is still running.
	 */
	boolean isRunning();

	/**
	 * Checks if this operation deadlocks.
	 * @throws DeadlockIdentifiedException
	 */
	void check() throws DeadlockIdentifiedException;

	@Override
	void close();
}
