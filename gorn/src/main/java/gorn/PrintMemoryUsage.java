package gorn;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrintMemoryUsage implements Runnable {

	private final Logger log = Logger.getLogger("GORN");
	private final Level level;
	private PrintMemoryUsage(Level level) {
		this.level = level;
	}

	public void run() {
		Runtime rt = Runtime.getRuntime();
		long usedMemory = rt.totalMemory() - rt.freeMemory();
		log.log(level, "MEMORY USED " + usedMemory);
	}

	public static void printUsage(Level level, int period, TimeUnit unit) {
		ScheduledExecutorService s = Executors.newScheduledThreadPool(1);
		s.scheduleAtFixedRate(new PrintMemoryUsage(level), 0, period, unit);
	}
}
