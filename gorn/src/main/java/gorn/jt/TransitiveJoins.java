package gorn.jt;

import com.carrotsearch.hppc.ObjectStack;
import gorn.sif.KnownSet;
import gorn.util.Link;

import java.util.Collection;

/**
 * Implementation of the TransitiveJoins algorithm.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class TransitiveJoins<T> implements KnownSet<T> {
	private TaskId id;

	public TransitiveJoins() {
		id = TaskId.fromArray(0);
	}

	protected TransitiveJoins(TaskId id) {
		this.id = id;
	}

	@Override
	public void fork(T task) {
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
	    // nothing to do
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		if (knownSet == null) {
			return false;
		}
        TransitiveJoins<T> other = (TransitiveJoins<T>) knownSet;
        return this.id.canJoinTransitively(other.id);
	}


	@Override
	public Collection<T> asCollection() {
        throw new UnsupportedOperationException();
	}

	@Override
	public TransitiveJoins<T> createChild() {
		return new TransitiveJoins<T>(id.fork());
	}

	@Override
	public void clear() {
	    // Nothing to do
	}

	@Override
	public void setTask(T task) {
	}

    @Override
    public String toString() {
        return id.toString();
    }
}
