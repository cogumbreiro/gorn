package gorn.jt;

import java.util.Arrays;

/**
 * Creates a task id.
 * @author Tiago Cogumbreiro
 *
 */
public class TaskId {
	private final int[] components;
	private int time;
	private final int lastId;

	public TaskId(int[] components) {
		this.components = components;
		this.lastId = components[components.length - 1];
	}

	private int commonPrefix(TaskId other) {
		int prefix;
		final int minLen = Math.min(components.length, other.components.length);
		for (prefix = 0; prefix < minLen && components[prefix] == other.components[prefix]; prefix++) {

		}
		return prefix;
	}

    public boolean canJoinTransitively(TaskId other) {
        // direct child
        int idx = commonPrefix(other);
        if (idx >= this.components.length) {
            return this.components.length <= other.components.length;
        }
        return idx < other.components.length && this.components[idx] > other.components[idx];
    }

	public boolean canJoin(TaskId other) {
		if (other.components.length > this.components.length + 1) {
			return false;
		}
		int prefix = this.commonPrefix(other);

		// direct child
		if (prefix == this.components.length) {
			return true;
		}

		// not a child
		if (prefix == 0 || prefix + 1 != other.components.length) {
			return false;
		}
		// ancestor's child
		return other.components.length > prefix && other.components[prefix] < this.components[prefix];
	}
	
	private static int greaterThanPrefix(int[] l1, int[] l2) {
		final int totalSize = Math.min(l1.length, l2.length);
		for (int i = 0; i < totalSize; i++) {
			int cmp = l1[i] - l2[i];
			if (cmp > 0) {
				return i - 1;
			}
			if (cmp < 0) {
				return -1;
			}
		}
		return l1.length < l2.length ? l1.length - 1 : -1;
	}

	public boolean cannotJoin(TaskId other) {
		return greaterThanPrefix(components, other.components) == -1;
	}
	
	public int[] getBuffer() {
		return components;
	}
	
	public TaskId fork() {
		int[] newBuffer = Arrays.copyOf(components, components.length + 1);
		newBuffer[components.length] = time;
		time++;
		return new TaskId(newBuffer);
	}
	
	@Override
	public String toString() {
		return Arrays.toString(components);
	}
	
	public static TaskId fromArray(int...tid) {
		return new TaskId(tid);
	}
}
