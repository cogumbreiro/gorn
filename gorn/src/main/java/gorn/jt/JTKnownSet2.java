package gorn.jt;

import com.carrotsearch.hppc.ObjectStack;
import gorn.sif.KnownSet;
import gorn.util.Link;

import java.util.Collection;

/**
 * The basic known-set uses a global and immutable Knowledge Graph to test set
 * membership.
 * 
 * @author Tiago Cogumbreiro
 *
 */
public class JTKnownSet2<T> implements KnownSet<T> {
	private TaskId id;
    private Link<JTKnownSet2<T>> joins = null;

	public JTKnownSet2() {
		id = TaskId.fromArray(0);
	}

	protected JTKnownSet2(TaskId id, Link<JTKnownSet2<T>> joins) {
		this.id = id;
        this.joins = joins;
	}

	@Override
	public void fork(T task) {
	}

	@Override
	public void join(T task, KnownSet<T> knownSet) {
		JTKnownSet2<T> other = (JTKnownSet2<T>) knownSet;
        joins = new Link<>(other, joins);
	}

	@Override
	public boolean canJoin(T task, KnownSet<T> knownSet) {
		if (knownSet == null) {
			return false;
		}
        JTKnownSet2<T> other = (JTKnownSet2<T>) knownSet;
        return canJoin(this, other.id);
	}

	private static <T> boolean canJoin(JTKnownSet2<T> first, TaskId task) {
        ObjectStack<JTKnownSet2<T>> toProcess = new ObjectStack<>();
        toProcess.push(first);
        while (toProcess.size() > 0) {
            JTKnownSet2<T> k = toProcess.pop();
			if (k.id.cannotJoin(task)) {
				continue;
			}
            if (k.id.canJoin(task)) {
                return true;
            }
            for (Link<JTKnownSet2<T>> link = k.joins; link != null; link = link.next) {
                JTKnownSet2<T> element = link.element;
                toProcess.add(element);
            }
        }
        return false;
    }



	@Override
	public Collection<T> asCollection() {
        throw new UnsupportedOperationException();
	}

	@Override
	public JTKnownSet2<T> createChild() {
		return new JTKnownSet2<T>(id.fork(), joins);
	}

	@Override
	public void clear() {
		joins = null;
	}

	@Override
	public void setTask(T task) {
	}
}
