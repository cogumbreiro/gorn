package gorn.sif;


public class SnapKnownSetTest extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new SnapKnownSet<Integer>();
	}

}
