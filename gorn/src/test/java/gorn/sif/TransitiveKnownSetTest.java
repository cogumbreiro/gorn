package gorn.sif;

import gorn.jt.TransitiveJoins;

public class TransitiveKnownSetTest extends AbstractTransitiveKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new TransitiveJoins<>();
	}

}
