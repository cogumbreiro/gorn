package gorn.sif;

public class BasicKnownSetTest extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new BasicKnownSet<Integer>();
	}

}
