package gorn.sif;

public class VCKnownSetTest extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new VCKnownSet<Integer>();
	}

}
