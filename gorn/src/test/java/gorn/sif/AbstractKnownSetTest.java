package gorn.sif;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractKnownSetTest {

	private static final int ROOT = -1;

	private static <T> KnownSet<T> fork(KnownSet<T> k, T task) {
		KnownSet<T> child = k.createChild();
		child.setTask(task);
		k.fork(task);
		return child;
	}

	KnownSet<Integer> root;
	Map<Integer, KnownSet<Integer>> ksChildren;
	Map<KnownSet<Integer>, Integer> ids;
	int lastId;

	abstract protected KnownSet<Integer> create();

	private KnownSet<Integer> doFork(KnownSet<Integer> k, int id) {
		KnownSet<Integer> child = fork(k, id);
		ksChildren.put(id, child);
		ids.put(child, id);
		return child;
	}
	
	private KnownSet<Integer> doFork(KnownSet<Integer> k) {
		lastId++;
		return doFork(k, new Integer(lastId));
	}
	
	private void assertJoin(KnownSet<Integer> ks, Integer id) {
		KnownSet<Integer> other = ksChildren.get(id);
		String msg = "canJoin(" + id + ")\nTHIS=" + ks + "\nOTHER=" + other;
		assertTrue(msg, ks.canJoin(id, other));
		ks.join(id, other);
	}

	private void assertJoin(KnownSet<Integer> ks, KnownSet<Integer> other) {
		assertJoin(ks, ids.get(other));
	}

	private void assertCannotJoin(KnownSet<Integer> ks, Integer id) {
		KnownSet<Integer> other = ksChildren.get(id);
		String msg = "cannotJoin(" + id + ")\nTHIS=" + ks + "\nOTHER=" + other;
		assertFalse(msg, ks.canJoin(id, other));
	}

	private void assertCannotJoin(KnownSet<Integer> ks, KnownSet<Integer> other) {
		assertCannotJoin(ks, ids.get(other));
	}

	@Before
	public void setUp() {
		lastId = ROOT + 1;
		root = create();
		root.setTask(ROOT);
		ksChildren = new HashMap<>();
		ksChildren.put(ROOT, root);
		ids = new HashMap<>();
		ids.put(root, ROOT);
	}

	@Test
	public void forkMany() {
		int TOTAL = 20;
		for (int i = 0; i < TOTAL; i++) {
			doFork(root, i);
		}
		for (int i = 0; i < TOTAL; i++) {
			assertJoin(root, i);
		}
		for (int i = TOTAL; i < 2 * TOTAL; i++) {
			assertCannotJoin(root, i);
		}
	}

	@SuppressWarnings({ "unchecked", "unused" })
	@Test
	public void forkJoin() {
		int i;
		int childrenCount = 5;
		KnownSet<Integer>[] children = new KnownSet[childrenCount];
		for (i = 0; i < childrenCount; i++) {
			children[i] = doFork(root, i * 10);
			assertJoin(root, i*10);
		}
		int childId = 0;
		for (KnownSet<Integer> child : children) {
			for (i = 1; i < childrenCount; i++) {
				int id = childId * 10 + i;
				KnownSet<Integer> ks = children[i];
				doFork(ks, id);
				assertJoin(ks, id);
			}
			childId++;
		}
		i = 0;
		for (KnownSet<Integer> child : children) {
			assertJoin(root, i*10);
			i++;
		}
		childId = 0;
		for (KnownSet<Integer> child : children) {
			for (i = 1; i < childrenCount; i++) {
				assertJoin(root, i*10);
			}
			childId++;
		}
	}

	@Test
	public void forkMany2() {
		int count = 10;
		for (int i = 0; i < count; i++) {
			doFork(root, i);
		}
		for (int i = 0; i < count; i++) {
			assertJoin(root, i);
		}
		for (int i = count; i < count + 10; i++) {
			assertCannotJoin(root, i);
		}
	}

	@Test
	public void joinRepeatedly() {
		KnownSet<Integer> ks = doFork(root, 1);
		for (int i = 0; i < 100; i++) {
			assertJoin(root, ks);
		}
	}

	@Test
	public void joinGrandChild() {
		KnownSet<Integer> child = doFork(root, 1);
		doFork(child, 2);
		assertCannotJoin(root, 2);
		assertJoin(root, 1);
		assertJoin(root, 2);
		assertJoin(root, 2);
		assertJoin(root, 1);
		assertJoin(root, 1);
		assertJoin(root, 2);
	}

	@Test
	public void example1() {
		KnownSet<Integer> n0 = doFork(root);
		assertCannotJoin(n0, root);
		assertJoin(root, n0);
	}

	@Test
	public void example3() {
		assertCannotJoin(root, 1);
	}

	@Test
	public void example2() {
		KnownSet<Integer> n0 = doFork(root, 1);
		doFork(n0, 2);
		assertJoin(n0, 2);
	}
	
	@Test
	public void example4() {
		KnownSet<Integer> n0 = doFork(root);
		assertCannotJoin(n0, root);
	}

	@Test
	public void example5() {
		KnownSet<Integer> n1 = doFork(root);
		KnownSet<Integer> n2 = doFork(root);
		assertCannotJoin(n2, root);
		assertCannotJoin(n1, root);
		assertCannotJoin(n1, n2);
	}
	
	@Test
	public void bug1() {
		// root -> n0
		KnownSet<Integer> n0 = doFork(root);
		// n0 -> n1
		KnownSet<Integer> n1 = doFork(n0);
		// n1 -> n2
		KnownSet<Integer> n2 = doFork(n1);
		// root -> m0
		KnownSet<Integer> m0 = doFork(root);

		// n0 join n1
		// m0 join n0
		// m0 join n2
		assertCannotJoin(n1, root);
		assertCannotJoin(n1, n0);
		assertCannotJoin(n1, m0);
		assertCannotJoin(n2, root);
		assertCannotJoin(n2, n1);
		assertCannotJoin(n2, m0);
		assertCannotJoin(m0, root);
		assertCannotJoin(m0, n1);
		assertCannotJoin(m0, n2);
		assertCannotJoin(root, n1);
		assertCannotJoin(root, n2);
		assertJoin(n0, n1); // join with child
		assertJoin(m0, n0); // join with sibling
		assertJoin(m0, n2);
	}

	private void fib(KnownSet<Integer> set, int fuel) {
		if (fuel < 1) {
			return;
		}
		KnownSet<Integer> f1 = doFork(set);
		fib(f1, fuel - 1);
		KnownSet<Integer> f2 = doFork(set);
		fib(f2, fuel - 2);
		assertJoin(set, f1);
		assertJoin(set, f2);
	}

	private void run1(int count) {
		fib(root, count);
		for (int j = 1; j <= lastId; j++) {
			assertTrue(root.canJoin(j, ksChildren.get(j)));
		}

		for (int j = lastId; j >= 1; j--) {
			assertTrue(root.canJoin(j, ksChildren.get(j)));
			root.join(j, ksChildren.get(j));
		}
	}

	@Test
	public void testFib() {
		run1(3);
	}
}
