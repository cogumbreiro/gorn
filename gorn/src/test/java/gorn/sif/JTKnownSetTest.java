package gorn.sif;

import gorn.jt.JTKnownSet2;

public class JTKnownSetTest extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new JTKnownSet2<>();
	}

}
