package gorn.sif.snapshotset;

import static org.junit.Assert.*;
import gorn.sif.snapshotset.SnapshotArrayList;
import gorn.sif.snapshotset.SnapshotList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class SnapshotArrayListTest extends AbstractSnapshotListTest {
	@Override
	protected SnapshotList<String> createList() {
		return new SnapshotArrayList<>();
	}

}
