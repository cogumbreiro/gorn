package gorn.sif.snapshotset;

import static org.junit.Assert.*;
import gorn.sif.snapshotset.Snapshot;

import org.junit.Test;

public class SnapshotTest {

	@Test
	public void testMatches() {
		Snapshot<String> snap1 = new Snapshot<>(1, "foo");
		assertTrue(snap1.contains("foo"));
		assertFalse(snap1.contains("bar"));
	}

	@Test
	public void testIsVisible() {
		Snapshot<String> snap1 = new Snapshot<>(1, "foo");
		assertFalse(snap1.isVisibleAt(0));
		assertTrue(snap1.isVisibleAt(1));
		assertTrue(snap1.isVisibleAt(2));
	}

}
