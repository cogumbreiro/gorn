package gorn.sif.snapshotset;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import gorn.sif.snapshotset.Snapshot;
import gorn.sif.snapshotset.SnapshotList;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

public abstract class AbstractSnapshotListTest {
	protected abstract SnapshotList<String> createList();
	
	@Test
	public void testAdd() {
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		SnapshotList<String> l = createList();
		l.add(s1);
		l.add(s2);
		assertEquals(Arrays.asList(s1, s2), Util.asList(l));
	}

	@Test
	public void testAddMany() {
		ArrayList<Snapshot<String>> expected = new ArrayList<Snapshot<String>>();
		SnapshotList<String> l = createList();
		for (int i = 0; i < 9; i++) {
			Snapshot<String> s = new Snapshot<>(i, "" + i);
			l.add(s);
			expected.add(s);
		}
		assertEquals(expected, Util.asList(l));
	}
	
	@Test
	public void testContains1() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		assertTrue(l.contains("foo"));
		assertTrue(l.contains("bar"));
		assertFalse(l.contains("baz"));
	}

	@Test
	public void testContains2() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		assertTrue(l.contains(1, "foo"));
		assertFalse(l.contains(1, "bar"));
		assertFalse(l.contains(1, "baz"));
		assertTrue(l.contains(2, "foo"));
		assertTrue(l.contains(2, "bar"));
		assertFalse(l.contains(2, "baz"));
		assertTrue(l.contains(3, "foo"));
		assertTrue(l.contains(3, "bar"));
		assertFalse(l.contains(3, "baz"));
	}
/*
	@Test
	public void testFilter1() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		Snapshot<String> s3 = new Snapshot<>(3, "riz");
		l.add(s3);
		assertEquals(Arrays.asList("foo"), Util.asList(l.filter(1)));
	}

	@Test
	public void testFilter2() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		Snapshot<String> s3 = new Snapshot<>(3, "riz");
		l.add(s3);
		assertEquals(Arrays.asList("foo", "bar"), Util.asList(l.filter(2)));
	}
	
	@Test
	public void testFilter3() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		Snapshot<String> s3 = new Snapshot<>(3, "riz");
		l.add(s3);
		assertEquals(Arrays.asList("foo", "bar", "riz"), Util.asList(l.filter(3)));
	}

	@Test
	public void testFilter4() {
		SnapshotList<String> l = createList();
		Snapshot<String> s1 = new Snapshot<>(1, "foo");
		l.add(s1);
		Snapshot<String> s2 = new Snapshot<>(2, "bar");
		l.add(s2);
		Snapshot<String> s3 = new Snapshot<>(3, "riz");
		l.add(s3);
		assertEquals(Arrays.asList("foo", "bar", "riz"), Util.asList(l.filter(99)));
	}
*/
}
