package gorn.sif.snapshotset;

import gorn.sif.snapshotset.SnapshotLinkedList;
import gorn.sif.snapshotset.SnapshotList;

public class SnapshotLinkedListTest extends AbstractSnapshotListTest {
	@Override
	protected SnapshotList<String> createList() {
		return new SnapshotLinkedList<>();
	}

}
