package gorn.sif.snapshotset;

import static org.junit.Assert.*;

import gorn.sif.snapshotset.Container;
import gorn.sif.snapshotset.SnapshotSetBucket;

import org.junit.Test;

public class SnapshotSetTest {

	@Test
	public void addContains() {
		SnapshotSetBucket<Integer> set = new SnapshotSetBucket<Integer>();
		for (int i = 1; i <= 3; i++) {
			assertFalse(set.contains(i));
		}
		set.add(1);
		assertTrue(set.contains(1));
		assertFalse(set.contains(4));
		set.add(2);
		assertTrue(set.contains(2));
		assertFalse(set.contains(4));
		set.add(3);
		assertTrue(set.contains(3));
		assertFalse(set.contains(4));
		for (int i = 1; i <= 3; i++) {
			assertTrue(set.contains(i));
		}
		assertFalse(set.contains(4));
	}

	@Test
	public void createView() {
		SnapshotSetBucket<Integer> set = new SnapshotSetBucket<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
		assertFalse(set.contains(4));
		Container<Integer> set2 = set.createSnapshot();
		for (int i = 1; i <= 3; i++) {
			assertTrue("contains(" + i + ")", set2.contains(i));
			assertTrue(set.contains(i));
		}
		assertFalse(set.contains(4));
		assertFalse(set2.contains(4));
	}

	@Test
	public void createViewAddInOriginal() {
		SnapshotSetBucket<Integer> set = new SnapshotSetBucket<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
		Container<Integer> set2 = set.createSnapshot();
		for (int i = 0; i < 999; i++) {
			set.add(i);			
		}
		assertTrue("contains(4)", set.contains(4));
		assertFalse(set2.contains(4));
	}
	
}
