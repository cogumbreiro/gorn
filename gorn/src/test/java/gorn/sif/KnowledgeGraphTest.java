package gorn.sif;

import static org.junit.Assert.*;

import static org.junit.Assert.assertEquals;

import static gorn.sif.knowngraph.KnowledgeGraph.asCollection;

import java.util.Arrays;

import gorn.sif.knowngraph.KnowledgeGraph;
import org.junit.Test;

public class KnowledgeGraphTest {
	@Test
	public void empty() {
		KnowledgeGraph<Integer> g = new KnowledgeGraph<Integer>();
		assertTrue(asCollection(g).isEmpty());
	}

	@Test
	public void add1() {
		KnowledgeGraph<Integer> g = new KnowledgeGraph<Integer>();
		g = g.add(1);
		assertEquals(Arrays.asList(1), asCollection(g));
	}

	@Test
	public void add2() {
		KnowledgeGraph<Integer> g = new KnowledgeGraph<Integer>();
		g = g.add(1);
		g = g.add(2);
		assertEquals(Arrays.asList(2,1), asCollection(g));
	}

}
