package gorn.sif;

import gorn.itc.ITCKnownSet;

public class ITCKnownSetTest extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new ITCKnownSet<>();
	}

}
