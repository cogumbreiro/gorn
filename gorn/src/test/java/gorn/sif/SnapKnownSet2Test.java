package gorn.sif;


public class SnapKnownSet2Test extends AbstractKnownSetTest {

	@Override
	protected KnownSet<Integer> create() {
		return new SnapKnownSet2<Integer>(false);
	}

}
