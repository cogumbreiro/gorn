package gorn;

import static org.junit.Assert.*;
import gorn.Defaults.Flag;
import gorn.sif.SnapKnownSet;

import org.junit.Test;

public class DefaultsTest {

	@Test
	public void snapshot() {
		Defaults.resetFlags();
		Defaults.setSifStrategy(Defaults.SifStrategy.SNAPSHOT_SET);
		assertEquals(SnapKnownSet.class, Defaults.createKnownSet().getClass());
	}

}
