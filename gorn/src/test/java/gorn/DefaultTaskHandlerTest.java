package gorn;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class DefaultTaskHandlerTest {
	
	private static SynchronizationChecker<Integer> DEFAULT = new SynchronizationChecker<Integer>() {
        @Override
        public SynchronizationResult checkBlocked(Integer synch) {
            return SynchronizationResult.UNKNOWN;
        }
    };

	@Test
	public void lifecycle1() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		assertEquals(new Integer(1), task1.getTask());
		task1.onEnd();
	}

	private TaskHandler<Integer> create() {
	    Defaults.setFlag(Defaults.Flag.SIF, true);
		return Defaults.createTaskHandler();
	}

	@Test
	public void lifecycle2() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		TaskHandler<Integer> task2 = task1.createChild();
		task2.setTask(2);
		assertEquals(new Integer(2), task2.getTask());
		task1.onEnd();
	}

	/**
	 * Task1 forks task2. Task1 joins with task2.
	 */
	@Test
	public void safe1() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		TaskHandler<Integer> task2 = task1.createChild();
		task2.setTask(2);
		task1.onSpawn(2);
		task1.beforeJoin(task2, DEFAULT);
		task1.afterJoin(task2);
		task1.onEnd();
	}

    public void assertUnsafeBeforeJoin(TaskHandler<Integer> source, TaskHandler<Integer> target) {
        try {
            // task is awaiting on itself
            try (OngoingWait ongoingWait = source.beforeJoin(target, DEFAULT)) {
                assertTrue("Expecting an ongoing object to block on but got null", ongoingWait != null);
                while (ongoingWait.isRunning()) {
                    ongoingWait.check();
                }
            }
            fail();
        } catch (DeadlockIdentifiedException e) {
            // OK
        }
    }

    public void assertSafeBeforeJoin(TaskHandler<Integer> source, TaskHandler<Integer> target) {
        try {
            // task is awaiting on itself
            try (OngoingWait ongoingWait = source.beforeJoin(target, DEFAULT)) {
                if (ongoingWait == null) {
                    // completely safe!
                    return;
                }
                while (ongoingWait.isRunning()) {
                    ongoingWait.check();
                }
            }
            // OK
        } catch (DeadlockIdentifiedException e) {
            fail();
        }
    }

    /**
	 * Task1 deadlocks with itself.
	 */
	@Test
	public void deadlock1() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		assertUnsafeBeforeJoin(task1, task1);
		task1.afterJoin(task1);
		task1.onEnd();
	}

	/**
	 * Task1 forks task2. Task1 joins with task2.
	 */
	@Test
	public void deadlock2() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		TaskHandler<Integer> task2 = task1.createChild();
		task2.setTask(2);
		task1.onSpawn(2);
		assertSafeBeforeJoin(task1, task2);
        assertUnsafeBeforeJoin(task2, task1);
		task1.afterJoin(task2);
		task2.afterJoin(task1);
		task1.onEnd();
		task2.onEnd();
	}

	@Test
	public void bug1() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		TaskHandler<Integer> task2 = task1.createChild();
		task2.setTask(2);
		task1.onSpawn(2);
		assertSafeBeforeJoin(task1, task2);
        assertUnsafeBeforeJoin(task2, task1);
        task2.afterJoin(task2);
		task2.onEnd();
		task1.afterJoin(task1);
	}

	@Test
	public void bug2() {
		TaskHandler<Integer> task1 = create();
		task1.setTask(1);
		TaskHandler<Integer> task2 = task1.createChild();
		task2.setTask(2);
		task1.onSpawn(2);

		TaskHandler<Integer> task3 = task1.createChild();
		task3.setTask(3);
		task1.onSpawn(3);

		assertSafeBeforeJoin(task1, task2);
        assertSafeBeforeJoin(task3, task2);
        assertUnsafeBeforeJoin(task2, task3);
        task2.afterJoin(task3);
		task2.onEnd();
		task3.afterJoin(task2);
		task3.onEnd();
		task1.afterJoin(task1);
	}
}
