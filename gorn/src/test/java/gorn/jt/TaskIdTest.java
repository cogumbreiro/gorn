package gorn.jt;

import static org.junit.Assert.*;

import static gorn.jt.TaskId.fromArray;

import org.junit.Test;

import java.util.Arrays;

public class TaskIdTest {

	@Test
	public void creating() {
		assertTaskEquals(fromArray(1,2,3,4,5), 1,2,3,4,5);
	}

	@Test
	public void forking() {
		TaskId t1 = fromArray(1);
		assertTaskEquals(t1, 1);
		TaskId t2 = t1.fork();
		assertTaskEquals(t2, 1, 0);
		TaskId t3 = t1.fork();
		assertTaskEquals(t3, 1, 1);
		assertTaskEquals(t3.fork(), 1,1,0);
		assertTaskEquals(t3.fork(), 1,1,1);
		assertTaskEquals(t3.fork(), 1,1,2);
	}

	private static void assertTaskEquals(TaskId obtained, int...expected) {
		assertArrayEquals(expected, obtained.getBuffer());
	}
	
	@Test
	public void cannotJoin() {
		assertTrue(fromArray(3).cannotJoin(fromArray(1)));
		assertTrue(fromArray(3).cannotJoin(fromArray(1,3,4)));
		assertTrue(fromArray(1, 1, 1).cannotJoin(fromArray(1, 1, 2)));
		assertTrue(fromArray(1, 3, 1).cannotJoin(fromArray(1, 3)));
		assertTrue(fromArray(1, 1, 2).cannotJoin(fromArray(1, 3)));
		assertTrue(fromArray(1, 2).cannotJoin(fromArray(1, 3)));
		assertTrue(fromArray(1, 1).cannotJoin(fromArray(1, 3)));
	}

	@Test
	public void canJoinOk() {
		assertTrue(fromArray(1,3,2,5).canJoin(fromArray(1,2)));
		assertTrue(fromArray(1,3,2,5).canJoin(fromArray(1,3,1)));
		assertTrue(fromArray(1,3,2,5).canJoin(fromArray(1,3,2,4)));
		assertTrue(fromArray(1,3,2,5).canJoin(fromArray(1,3,2,5,6)));
	}

	@Test
	public void bug1() {
		assertFalse(fromArray(0, 1).canJoin(fromArray(0, 0, 0)));
	}
}
